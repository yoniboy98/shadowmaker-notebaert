# Shadowmaker
Vind ons op https://shadowmaker.be/
Vind onze PWA op https://brentnot.be/Shadowmaker/index.html

1) Huidige problemen
- Papieren versie (zie foto's in Pictures)
--> Stappen worden overgeslagen
--> Moeilijk aanpasbaar
--> Onoverzichtelijk
--> Fouten worden gemaakt
--> Soms niet leesbaar
--> Bijhouden van veel papier

2) Doelstellingen
- Papieren versie omvormen naar een PWA
- Op alle soorten toestellen aanwezig
- Offline data doorsturen
- Online data opslaan in firestore
- Data bijhouden in LocalStorage zodat het project op een ander tijdstip kan verder gezet worden

3) Documentatie:
3.1) PROJECT AANMAKEN
Voor het aanmaken van een nieuw project klik je op de afbeelding boven “Een nieuw project aanmaken”. Hierdoor zal er een popup tevoorschijn komen die informatie over het project en de klant zal vragen. Na het invullen van alle gegevens kan het project aangemaakt worden door de knop “Maak project aan”. Dan wordt de 2de popup open gedaan waar je in dat project verschillende producten kan toevoegen. Wanneer je klikt op de knop “Voeg afbeeldingen toe” dan zal het product aangemaakt worden en kan je voor dat product afbeeldingen toevoegen. Nadien kan er opnieuw een product toegevoegd worden in hetzelfde project.

3.2) PROJECTEN
Bij het bekijken van alle projecten kan je bovenaan zoeken naar een specifiek project. Vervolgens zie je een lijst van projecten die gebookmarked zijn en dus vanboven in de lijst op het scherm te zien zullen zijn. Alle projecten die bezig zijn zullen in de lijst daaronder getoont worden volgens datum (van oud naar nieuw). Je kan een project toevoegen aan bookmark, afgewerkt of verwijderen door te sliden naar links op het gekozen project. Ook kan je een bookmarked project afwerken of terug toevoegen aan bezig door eveneens te sliden naar links. Wanneer een project afgewerkt is kan je deze verwijderen of terug toevoegen aan bezig als het toch niet afgewerkt zou zijn.
Na het klikken op een project krijg je alle gegevens van het project te zien. Je kan deze gegevens bewerken door rechtsboven op het bewerk icoontje te klikken en dan je aanpassingen op te slaan. Bij het sliden naar links of klikken op lijst producten krijg je een overzicht van alle producten in dat project. Wanneer je op het aanmaak icoontje klikt kan je een nieuw product met afbeeldingen toevoegen aan het project. Bij het sliden naar links op een product kan je deze verwijderen. Na het klikken op een product kan je alle gegevens hierover vinden, alsook de afbeeldingen in een slider vorm onderaan. Wil je dit product wijzigen of een nieuwe afbeelding toevoegen kan je dit doen door rechtsboven op het bewerk icoontje te klikken en je aanpassingen op te slaan.

3.3) KLANTEN
Bij het bekijken van alle klanten kan je bovenaan zoeken naar een specifieke klant. Vervolgens zie je een lijst van alle klanten gesorteerd via achternaam. Bij het sliden naar links kan je de route naar die klant openen met google maps. Na het klikken op een klant kan je zijn gegevens bekijken en wijzigen, alsook welk project bij de klant hoort.
