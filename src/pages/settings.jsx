import React from 'react';
import {
  Page,
  Navbar,
  List,
  ListItem,
  Block,
  NavTitle,
  Subnavbar,
  Searchbar,
  theme
} from 'framework7-react';
import KlantList from '../components/KlantList';

const SettingsPage = () => {
  return (
    <Page name="settings"> 
      <Navbar>
        <NavTitle>Overzicht Klanten</NavTitle>
      </Navbar>
      
      {/* Zoeken naar een klant */}
      <Subnavbar inner={false}>
        <Searchbar
          searchContainer=".search-list"
          searchIn=".item-title"
          disableButton={!theme.aurora}>
        </Searchbar>
      </Subnavbar>

      {/* Geen klant gevonden */}
      <List className="searchbar-not-found">
        <ListItem id="titleOvereenkomsten" title="Geen overeenkomsten gevonden."></ListItem>
      </List>

      <Block>
        {/* Lijst van klanten */}
        <KlantList/>
      </Block>
    </Page>
  );
};
export default SettingsPage;