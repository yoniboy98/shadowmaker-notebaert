import React from 'react';
import { 
  Page,
  Navbar,
  List,
  ListItem,
  Block,
  Subnavbar,
  Searchbar,
  theme,
  NavTitle
} from 'framework7-react';
import ProjectList from '../components/projectList';

const CatalogPage = () => {
  return (
    <Page name="catalog"> 
      <Navbar>
        <NavTitle>Overzicht Projecten</NavTitle>
      </Navbar>
      
      {/* Zoeken naar een project */}
      <Subnavbar inner={false}>
        <Searchbar
          searchContainer=".search-list"
          searchIn=".item-title"
          disableButton={!theme.aurora}>
        </Searchbar>
      </Subnavbar>

      {/* Geen project gevonden */}
      <List className="searchbar-not-found">
        <ListItem id="titleOvereenkomsten" title="Geen overeenkomsten gevonden."></ListItem>
      </List>

      <Block>
        {/* Lijst van projecten */}
        <ProjectList/>
      </Block>
    </Page>
  );
}
export default CatalogPage;