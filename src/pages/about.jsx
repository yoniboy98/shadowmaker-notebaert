import React from 'react';
import { 
  Page,
  Navbar,
  Block,
  BlockTitle
} from 'framework7-react';

const AboutPage = () => (
  <Page>
    <Navbar title="About" backLink="Back" />
    <BlockTitle>Over Shadowmaker</BlockTitle>
    <Block strong>
      <p>Aanmaken, bekijken, aanpassen en verwijderen van projecten.</p>
    </Block>
  </Page>
);
export default AboutPage;