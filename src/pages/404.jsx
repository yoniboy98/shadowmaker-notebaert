import React from 'react';
import { 
  Page,
  Navbar,
  Block
} from 'framework7-react';

const NotFoundPage = () => (
  <Page>
    <Navbar title="Niet gevonden" backLink="Back"/>
    <Block strong>
      <p>Sorry, de gevraagde pagina kon niet opgehaald worden. Probeer later opnieuw.</p>
    </Block>
  </Page>
);
export default NotFoundPage;