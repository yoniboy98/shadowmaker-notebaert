import React, { useState } from 'react';
import {
  Page,
  Navbar,
  NavTitle,
  Link,
  NavRight,
  Block,
  Popup
} from 'framework7-react';
import AanmakenProjectIcon from '../components/AanmakenProjectIcon';
import FirstAddProjectModal from '../components/FirstAddProjectModal';

const HomePage = () => {

  const [popupOpenedFirst, setPopupOpenedFirst] = useState(false);

  return (
    <Page name="home">
      <Navbar>
        <NavTitle>Shadowmaker</NavTitle>
      </Navbar>

      {/* Icon click foto */}
      <AanmakenProjectIcon/>

      {/* Popup na icon click */}
      <Popup
        className="addFirst-popup"
        opened={popupOpenedFirst}
        onPopupClosed={() => setPopupOpenedFirst(false)}
        tabletFullscreen>
        <Page>
          <Navbar title="Aanmaken Nieuw Project">
            <NavRight>
              <Link popupClose>Sluit</Link>
            </NavRight>
          </Navbar>
          <Block>
            {/* Inputvelden tonen informatie + producten */}
            <FirstAddProjectModal/>
          </Block>
        </Page>
      </Popup>
    </Page>
  )
};
export default HomePage;