// Import React and ReactDOM
import React from 'react';
import ReactDOM from 'react-dom';

// Import Framework7
import Framework7 from 'framework7/lite-bundle';

// Import Framework7-React Plugin
import Framework7React from 'framework7-react';

// Import Framework7 Styles
import 'framework7/framework7-bundle.css';

// Import Icons and App Custom Styles
import './css/icons.css';
import './css/app.css';

// Import App Component
import App from './components/app.jsx';

// Import Service Worker
import * as serviceWorker from './serviceWorkerRegistration';

// Init F7 React Plugin
Framework7.use(Framework7React)

// Mount React App
/*ReactDOM.render(
  React.createElement(App),
  document.getElementById('app'),
);*/

const renderReactDom = () => {
  ReactDOM.render(
    React.createElement(App),
    document.getElementById('app')
  );
};

if (window.cordova) {
  document.addEventListener('deviceready', () => {
    renderReactDom();
  }, false);
} else {
  renderReactDom();
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();

//bronnen: https://github.com/udacity/ud811 -- https://developers.google.com/web/fundamentals/primers/service-workers
//Service-worker
/*if ('serviceWorker' in navigator) {
  navigator.serviceWorker
   .register('/service-worker2.js')
   .then(function() { 
      console.log('Service Worker Registered'); 
    });
} else {
  console.log("Service Worker is not supported by browser.");
}*/

//Location
/*if ("geolocation" in navigator) {
  console.log("GPS available");
} else {
  console.log("GPS not available");
}*/
