import React, { useState, useEffect} from 'react';
import {
  SwipeoutActions,
  SwipeoutButton,
  List,
  ListItem,
  Icon,
  f7
} from 'framework7-react';
import { initializeApp } from 'firebase/app';
import { getFirestore, collection,  deleteDoc, doc,query, onSnapshot, orderBy,  where, updateDoc } from 'firebase/firestore';
import { enableIndexedDbPersistence } from "firebase/firestore"; 

//Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDqcQyhGE5MXMS0bTb9y-_NgEitwRYf_r8",
  authDomain: "shadowmaker-82b93.firebaseapp.com",
  projectId: "shadowmaker-82b93",
  storageBucket: "shadowmaker-82b93.appspot.com",
  messagingSenderId: "302524693982",
  appId: "1:302524693982:web:5ca9e54699821dcdb4714f",
  measurementId: "G-V9B33GZR83"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

enableIndexedDbPersistence(db).catch((err) => {
  if (err.code === 'failed-precondition') {
    // Multiple tabs open, persistence can only be enabled in one tab at a a time.
    alert('Project kan niet in meer dan 1 tab geopend worden. Sluit alle andere Shadowmaker tabs.');
  } else if (err.code === 'unimplemented') {
    // The current browser does not support all of the features required to enable persistence
    alert('Data kan niet offline opgehaald worden in deze browser.');
}
});

const ProjectList = () => {

  const [projecten, setProjecten] = useState([]);
  const [projectenBookmark, setProjectenBookmark] = useState([]);
  const [projectenAfgewerkt, setProjectenAfgewerkt] = useState([]);
  const [sizeBookmark, setSizeBookmark] = useState();
  const [sizeBezig, setSizeBezig] = useState();
  const [sizeAfgewerkt, setSizeAfgewerkt] = useState();

  //Projecten ophalen
  const getProjecten = async () => {
    const q = query(collection(db, "projecten"), orderBy("project", "asc"), where("bookmark", "==", "false"), where("status", "==", "niet afgewerkt"))
    onSnapshot(q, (doc) => {
      setProjecten(doc.docs.map((doc) => ({...doc.data(), id: doc.id})));
      setSizeBezig(doc.size);
    });
  };

  //Projecten ophalen met een bookmark
  const getProjectenBookmark = async () => {
    const q = query(collection(db, "projecten"), orderBy("project", "asc"), where("bookmark", "==", "true"))
    onSnapshot(q, (doc) => {
      setProjectenBookmark(doc.docs.map((doc) => ({...doc.data(), id: doc.id})));
      setSizeBookmark(doc.size);
    });
  };

  //Projecten ophalen die zijn afgewerkt
  const getProjectenAfgewerkt = async () => {
    const q = query(collection(db, "projecten"), orderBy("project", "asc"), where("bookmark", "==", "false"), where("status", "==", "afgewerkt"))
    onSnapshot(q, (doc) => {
      setProjectenAfgewerkt(doc.docs.map((doc) => ({...doc.data(), id: doc.id})));
      setSizeAfgewerkt(doc.size);
    });
  };

  useEffect(() => {
    getProjecten();
    getProjectenBookmark();
    getProjectenAfgewerkt();
  },[]);

  //Bookmark true zetten in project
  const addBookmark = async(id) => {
    const productReference = doc(db, "projecten", id);
    await updateDoc(productReference, {
      bookmark: "true",
    }).then(() => {
      //
    })
    .catch((error) => {
      alert('Kan bookmark niet toevoegen aan het project.');
    });
  }

  //Bookmark false zetten in project
  const deleteBookmark = async(id) => {
    const productReference = doc(db, "projecten", id);
    await updateDoc(productReference, {
      bookmark: "false",
    }).then(() => {
      //alert('Bookmark is verwijderd uit het project.');
    })
    .catch((error) => {
      alert('Kan bookmark niet verwijderen uit het project.');
    });
  }

  //Project toevoegen aan afgewerkt
  const addToAfgewerkt = async(id) => {
    const productReference = doc(db, "projecten", id);
    await updateDoc(productReference, {
      bookmark: "false",
      status: "afgewerkt",
    }).then(() => {
      //
    })
    .catch((error) => {
      alert('Kan project niet toevoegen aan afgewerkt.');
    });
  }

  //Project verwijderen uit afgewerkt
  const deleteAfgewerkt = async(id) => {
    const productReference = doc(db, "projecten", id);
    await updateDoc(productReference, {
      status: "niet afgewerkt",
    }).then(() => {
      //
    })
    .catch((error) => {
      alert('Kan afgewerkt niet verwijderen uit het project.');
    });
  }

  //Project verwijderen
  const deleteProject = async (id) => {
    await deleteDoc(doc(db, "projecten", id));
      alert('Project is verwijdert.');
  };
  
  //Projecten tonen met de knop verwijderen en bookmark
  return (
    <React.Fragment>
      {sizeBookmark === 0 ? "" : <List id="listProjecten" className="search-list searchbar-found" sortable>
        <p className="pCenter"><Icon f7="bookmark_fill"></Icon>BOOKMARK</p>
        {projectenBookmark.map((pro) => {
          return <ListItem key={pro.id} header="" title={pro.project} swipeout routeProps={pro} after={pro.datum} link="/details-project/:id/"> 
            <SwipeoutActions right>
              <SwipeoutButton className='swipeoutButton' color='blue' confirmText="Ben je zeker dat je de bookmark wilt verwijderen?" confirmTitle="Bookmark verwijderen" onClick={() => deleteBookmark(pro.id)}>Bezig</SwipeoutButton>
              <SwipeoutButton className='swipeoutButton' color='green' onClick={() => addToAfgewerkt(pro.id)}>Afgewerkt</SwipeoutButton>
            </SwipeoutActions>
          </ListItem>
        })}
      </List>}
      {sizeBezig === 0 ? "" : <List id="listProjecten" className="search-list searchbar-found" sortable>
        <p className="pCenter"><Icon f7="repeat"></Icon> BEZIG</p>
        {projecten.map((pro) => {
          return <ListItem key={pro.id} header="" title={pro.project} swipeout routeProps={pro} after={pro.datum} onSwipeoutDelete={() => deleteProject(pro.id)} link="/details-project/:id/"> 
            <SwipeoutActions right>
              <SwipeoutButton className='swipeoutButton' color='blue' onClick={() => addBookmark(pro.id)}>Bookmark</SwipeoutButton>
              <SwipeoutButton className='swipeoutButton' color='green' onClick={() => addToAfgewerkt(pro.id)}>Afgewerkt</SwipeoutButton>
              <SwipeoutButton className='swipeoutButton' delete confirmText="Ben je zeker dat je dit project wilt verwijderen?" confirmTitle="Project verwijderen">Verwijder</SwipeoutButton>
            </SwipeoutActions>
          </ListItem>
        })}
      </List>}
      {sizeAfgewerkt === 0 ? "" : <List id="listProjecten" className="search-list searchbar-found" sortable>
        <p className="pCenter"><Icon f7="lock_fill"></Icon>AFGEWERKT</p>
        {projectenAfgewerkt.map((pro) => {
          return <ListItem key={pro.id} header="" title={pro.project} swipeout routeProps={pro} after={pro.datum} onSwipeoutDelete={() => deleteProject(pro.id)} link="/details-project/:id/"> 
            <SwipeoutActions right>
              <SwipeoutButton className='swipeoutButton' color='blue' confirmText="Ben je zeker dat je dit project nog niet afgewerkt is?" confirmTitle="Project niet afgewerkt" onClick={() => deleteAfgewerkt(pro.id)}>Bezig</SwipeoutButton>
              <SwipeoutButton className='swipeoutButton' delete confirmText="Ben je zeker dat je dit project wilt verwijderen?" confirmTitle="Project verwijderen">Verwijder</SwipeoutButton>
            </SwipeoutActions>
          </ListItem>
        })}
      </List>}
    </React.Fragment>
  )
}
export default ProjectList;