import React, { useState, useEffect } from 'react';
import {
  List,
  ListItem,
  Page,
  Block,
  NavRight,
  Toolbar,
  Tab,
  Tabs,
  ListInput,
  Button,
  Navbar,
  Link,
  Popup,
  f7,
  Icon
} from 'framework7-react';
import { initializeApp } from 'firebase/app';
import { getFirestore, onSnapshot, doc, updateDoc, query} from 'firebase/firestore';

//Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDqcQyhGE5MXMS0bTb9y-_NgEitwRYf_r8",
  authDomain: "shadowmaker-82b93.firebaseapp.com",
  projectId: "shadowmaker-82b93",
  storageBucket: "shadowmaker-82b93.appspot.com",
  messagingSenderId: "302524693982",
  appId: "1:302524693982:web:5ca9e54699821dcdb4714f",
  measurementId: "G-V9B33GZR83"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

const DetailsKlantList = (props) => {

  //useState
  const [popupOpened, setPopupOpened] = useState(false);
  
  const [details, setDetails] = useState([]);



  const [newVoornaam, setNewVoornaam ] = useState("");
  const [newAchternaam, setNewAchternaam ] = useState("");
  const [newAdres, setNewAdres ] = useState("");
  const [newGemeente, setNewGemeente ] = useState("");
  const [newTelefoon, setNewTelefoon ] = useState("");
  const [newEmail, setNewEmail ] = useState("");



  //Klant updaten
  const UpdateKlant = (id) => {
    const projectReference = doc(db, "projecten", id);
    updateDoc(projectReference, {
      voornaam: newVoornaam || details[0],
      achternaam: newAchternaam || details[1],
      adres: newAdres || details[2],
      gemeente: newGemeente || details[3],
      telefoon: newTelefoon || details[4],
      email: newEmail || details[5]
    }).then(() => {
      alert('Klant is succesvol aangepast.');
    })
    .catch(() => {
      alert('Het is niet gelukt om de gegevens te bewerken.');
    });
  }

  //Gegevens ophalen
  useEffect(() => {
    const getDetails = async () => {
      const q = query(doc(db, "projecten", props.id));
      onSnapshot(q, (doc) => {
        const data = doc.data();
        setDetails([
          data.voornaam,
          data.achternaam,
          data.adres,
          data.gemeente,
          data.telefoon,
          data.email,
          data.project,
        ]);
      });
    }
    getDetails();
  },[]);
  
  return (
    <Page name="Details">
      <Navbar title="Klant Details" backLink>
        <NavRight>
          <Block>
            <Link fill popupOpen=".demo-popup" iconOnly={true}>
              <Icon f7="pencil_ellipsis_rectangle"></Icon>
            </Link>
          </Block>
        </NavRight>
      </Navbar>

      <Toolbar tabbar top>
        <Link tabLink="#tab-1" tabLinkActive>Gegevens</Link>
        <Link tabLink="#tab-2">Project</Link>
      </Toolbar>
   
      <Tabs swipeable className="tabGrid">
        <Tab id="tab-1" className="page-content" tabActive>
          {/* Klanten met details weergeven */}
          <Block>
            <List noHairlinesMd>
              <ListItem header="Voornaam" swipeout title={details[0]}></ListItem>

              <ListItem header="Achternaam" swipeout title={details[1]}></ListItem>

              <ListItem header="Adres" swipeout title={details[2]}></ListItem>

              <ListItem header="Gemeente" swipeout title={details[3]}></ListItem>

              <ListItem header="Telefoon" swipeout title={details[4]}></ListItem>

              <ListItem header="Email" swipeout title={details[5]}></ListItem>
            </List>
          </Block>
        </Tab>

        {/* Route naar de klant */}
        <Tab id="tab-2" className="page-content">
          <Block>
            <List noHairlinesMd>
              {/* Lijst van projecten van de aangeklikte klant */}
              <List noHairlinesMd>
                <ListItem header="Project" swipeout title={details[6]}></ListItem>
              </List>
            </List>
          </Block>
        </Tab>
      </Tabs>

      {/* Popup om details te bewerken */}
      <Popup
        className="demo-popup"
        opened={popupOpened}
        onPopupClosed={() => setPopupOpened(false)}>
        <Page>
          <Navbar title="Klant Bewerken">
            <NavRight>
              <Link popupClose>Sluit</Link>
            </NavRight>
          </Navbar>
          <Block>
            <List noHairlinesMd>
              <ListInput
                id="klantVoornaam"
                label="Voornaam klant:"
                defaultValue={details[0] || newVoornaam}
                onBlur={(event) => {setNewVoornaam(event.target.value)}}
                type="text"
                onInputClear={() => {setNewVoornaam("")}}
                errorMessage="Geen getallen of vreemde tekens aub!"
                required
                minlength={3}
                maxlength={50}
                validate
                pattern="[a-zA-Z, .'-]*"
                placeholder="Voornaam van de klant"
                clearButton>
              </ListInput>

              <ListInput
                id="klantAchternaam"
                label="Achternaam klant:"
                defaultValue={details[1] || newVoornaam}
                onBlur={(event) => {setNewAchternaam(event.target.value)}}
                type="text"
                errorMessage="Geen getallen of vreemde tekens aub!"
                required
                minlength={3}
                maxlength={50}
                validate
                onInputClear={() => {setNewAchternaam("")}}
                pattern="[a-zA-Z, .'-]*"
                placeholder="Achternaam van de klant"
                clearButton>
              </ListInput>

              <ListInput
                id="klantAdres"
                label="Adres:"
                defaultValue={details[2] || newAdres}
                onBlur={(event) => {setNewAdres(event.target.value)}}
                type="text"
                errorMessage="Voer een geldig adres in aub!"
                required
                validate
                minlength={3}
                onInputClear={() => {setNewAdres("")}}
                pattern="[a-zA-Z0-9, .'-]*"
                placeholder="Adres van de klant"
                clearButton>
              </ListInput>

              <ListInput
                id="klantGemeente"
                label="Gemeente:"
                defaultValue={details[3] || newGemeente}
                onBlur={(event) => {setNewGemeente(event.target.value)}}
                type="text"
                errorMessage="Voer een geldige gemeente in aub!"
                required
                onInputClear={() => {setNewGemeente("")}}
                validate
                minlength={3}
                pattern="[A-Za-z0-9, ]*"
                placeholder="De gemeente van de klant"
                clearButton>
              </ListInput>

              <ListInput
                id="klantTelefoon"
                label="Telefoon:"
                defaultValue={details[4] || newTelefoon}
                onBlur={(event) => {setNewTelefoon(event.target.value)}}
                type="tel"
                errorMessage="Voer een geldig telefoonnummer in aub!"
                required
                minlength={10}
                maxlength={12}
                validate
                pattern="[0-9+]*"
                onInputClear={() => {setNewTelefoon("")}}
                placeholder="De telefoonnummer van de klant"
                clearButton>
              </ListInput>

              <ListInput
                id="klantEmail"
                label="E-mail:"
                defaultValue={details[5] || newEmail}
                onBlur={(event) => {setNewEmail(event.target.value)}}
                type="email"
                errorMessage="Voer een geldig email in aub!"
                required
                validate
                onInputClear={() => {setNewEmail("")}}
                pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
                placeholder="Email van de klant"
                clearButton>
              </ListInput>
            </List>

            <Button fill onClick={() => UpdateKlant(props.id)}>Bewerken</Button>
          </Block>
        </Page>
      </Popup>
    </Page>
  )
}
export default DetailsKlantList;