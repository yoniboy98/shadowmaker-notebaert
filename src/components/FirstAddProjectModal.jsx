import React, { useState, useEffect } from 'react';
import {
  Button,
  List,
  Block,
  Progressbar,
  Popup,
  Page,
  Navbar,
  NavRight,
  Input,
  Link,
  Badge,
  ListItem,
  ListInput
} from 'framework7-react';
import { initializeApp } from 'firebase/app';
import { getFirestore, collection, addDoc, onSnapshot } from 'firebase/firestore';
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import alert from 'alert'

//Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDqcQyhGE5MXMS0bTb9y-_NgEitwRYf_r8",
  authDomain: "shadowmaker-82b93.firebaseapp.com",
  projectId: "shadowmaker-82b93",
  storageBucket: "shadowmaker-82b93.appspot.com",
  messagingSenderId: "302524693982",
  appId: "1:302524693982:web:5ca9e54699821dcdb4714f",
  measurementId: "G-V9B33GZR83"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const storage = getStorage(app);

const FirstAddProjectModal = () => {

  //LocalStorage (get) - https://blog.logrocket.com/using-localstorage-react-hooks/
  const [localProjectnaam] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Projectnaam"));
    return initialValue || "";
  });
  const [localProjectDatum] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Datum"));
    return initialValue || "";
  });
  const [localKlantVoornaam] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Voornaam"));
    return initialValue || "";
  });
  const [localKlantAchternaam] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Achternaam"));
    return initialValue || "";
  });
  const [localKlantAdres] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Adres"));
    return initialValue || "";
  });
  const [localKlantGemeente] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Gemeente"));
    return initialValue || "";
  });
  const [localKlantTelefoon] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Telefoon"));
    return initialValue || "";
  });
  const [localKlantEmail] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Email"));
    return initialValue || "";
  });
  const [localProjectBtw] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("BTW"));
    return initialValue || "";
  });
  
  const [localProjectProduct] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Product"));
    return initialValue || "";
  });
  const [localProjectType] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Type"));
    return initialValue || "";
  });
  const [localProjectAantal] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Aantal"));
    return initialValue || "";
  });
  const [localProjectMotor] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Motor"));
    return initialValue || "";
  });
  const [localProjectKleur] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Kleur"));
    return initialValue || "";
  });
  const [localProjectGebouw] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Gebouw"));
    return initialValue || "";
  });
  const [localProjectGebouw2] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Gebouw 2"));
    return initialValue || "";
  });
  const [localProjectParking] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Parking"));
    return initialValue || "";
  });
  const [localProjectMontage] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Montage"));
    return initialValue || "";
  });
  const [localProjectVliegraam] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Vliegraam"));
    return initialValue || "";
  });
  const [localProjectBallustrade] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Ballustrade"));
    return initialValue || "";
  });
  const [localProjectBevestinging] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Bevestiging"));
    return initialValue || "";
  });
  const [localProjectLadders] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Ladders"));
    return initialValue || "";
  });
  const [localProjectLaddersHoogte] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Hoogte ladders"));
    return initialValue || "";
  });
  const [localProjectVerdiep] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Verdiep"));
    return initialValue || "";
  });
  const [localProjectKabel] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Kabel"));
    return initialValue || "";
  });
  const [localProjectKabelafwerking] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Kabelafwerking"));
    return initialValue || "";
  });
  const [localProjectAutomatisatie] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Automatisatie"));
    return initialValue || "";
  });
  const [localProjectBereikbaarheid] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Bereikbaarheid"));
    return initialValue || "";
  });
  const [localProjectOpmerkingen] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Opmerkingen"));
    return initialValue || "";
  });

  const [localProductFoto] = useState(() => {
    const initialValue = JSON.parse(localStorage.getItem("Foto"));
    return initialValue || "";
  });

  //useState
  const [newProjectnaam, setNewProjectnaam] = useState(localProjectnaam);
  const [newDatum, setNewDatum] = useState(localProjectDatum);
  const [newVoornaam, setNewVoornaam] = useState(localKlantVoornaam);
  const [newAchternaam, setNewAchternaam] = useState(localKlantAchternaam);
  const [newAdres, setNewAdres] = useState(localKlantAdres);
  const [newGemeente, setNewGemeente] = useState(localKlantGemeente);
  const [newTelefoon, setNewTelefoon] = useState(localKlantTelefoon);
  const [newEmail, setNewEmail] = useState(localKlantEmail);
  const [newBtw, setNewBtw] = useState(localProjectBtw);

  const [newProduct, setNewProduct] = useState(localProjectProduct);
  const [newType, setNewType] = useState(localProjectType);
  const [newAantal, setNewAantal] = useState(localProjectAantal);
  const [newMotor, setNewMotor] = useState(localProjectMotor);
  const [newKleur, SetNewKleur] = useState(localProjectKleur);
  const [newGebouw, setNewGebouw] = useState(localProjectGebouw);
  const [newGebouw2, setNewGebouw2] = useState(localProjectGebouw2);
  const [newParking, setNewParking] = useState(localProjectParking);
  const [newMontage, setNewMontage] = useState(localProjectMontage);
  const [newVliegraam, setNewVliegraam] = useState(localProjectVliegraam);
  const [newBallustrade, setNewBallustrade] = useState(localProjectBallustrade);
  const [newBevestiging, setNewBevestiging] = useState(localProjectBevestinging);
  const [newLadders, setNewLadders] = useState(localProjectLadders);
  const [newLaddersHoogte, setNewLaddersHoogte] = useState(localProjectLaddersHoogte);
  const [newVerdiep, setNewVerdiep] = useState(localProjectVerdiep);
  const [newKabel, setNewKabel] = useState(localProjectKabel);
  const [newKabelafwerking, setNewKabelafwerking] = useState(localProjectKabelafwerking);
  const [newAutomatisatie, setNewAutomatisatie] = useState(localProjectAutomatisatie);
  const [newBereikbaarheid, setNewBereikbaarheid] = useState(localProjectBereikbaarheid);
  const [newOpmerking, setNewOpmerking] = useState(localProjectOpmerkingen);

  const [newFoto, setNewFoto] = useState(localProductFoto);
  
  //LocalStorage (set) - https://blog.logrocket.com/using-localstorage-react-hooks/
  useEffect(() => {
    localStorage.setItem("Projectnaam", JSON.stringify(newProjectnaam));
    localStorage.setItem("Datum", JSON.stringify(newDatum));
    localStorage.setItem("Voornaam", JSON.stringify(newVoornaam));
    localStorage.setItem("Achternaam", JSON.stringify(newAchternaam));
    localStorage.setItem("Adres", JSON.stringify(newAdres));
    localStorage.setItem("Gemeente", JSON.stringify(newGemeente));
    localStorage.setItem("Telefoon", JSON.stringify(newTelefoon));
    localStorage.setItem("Email", JSON.stringify(newEmail));
    localStorage.setItem("BTW", JSON.stringify(newBtw));
  }, [newProjectnaam, newDatum, newVoornaam, newAchternaam, newAdres, newGemeente, newTelefoon, newEmail, newBtw]);

  useEffect(() => {
    localStorage.setItem("Product", JSON.stringify(newProduct));
    localStorage.setItem("Type", JSON.stringify(newType));
    localStorage.setItem("Aantal", JSON.stringify(newAantal));
    localStorage.setItem("Motor", JSON.stringify(newMotor));
    localStorage.setItem("Kleur", JSON.stringify(newKleur));
    localStorage.setItem("Gebouw", JSON.stringify(newGebouw));
    localStorage.setItem("Gebouw 2", JSON.stringify(newGebouw2));
    localStorage.setItem("Parking", JSON.stringify(newParking));
    localStorage.setItem("Montage", JSON.stringify(newMontage));
    localStorage.setItem("Vliegraam", JSON.stringify(newVliegraam));
    localStorage.setItem("Ballustrade", JSON.stringify(newBallustrade));
    localStorage.setItem("Bevestiging", JSON.stringify(newBevestiging));
    localStorage.setItem("Ladders", JSON.stringify(newLadders));
    localStorage.setItem("Hoogte ladders", JSON.stringify(newLaddersHoogte));
    localStorage.setItem("Verdiep", JSON.stringify(newVerdiep));
    localStorage.setItem("Kabel", JSON.stringify(newKabel));
    localStorage.setItem("Kabelafwerking", JSON.stringify(newKabelafwerking));
    localStorage.setItem("Automatisatie", JSON.stringify(newAutomatisatie));
    localStorage.setItem("Bereikbaarheid", JSON.stringify(newBereikbaarheid));
    localStorage.setItem("Opmerking", JSON.stringify(newOpmerking));
  }, [newProduct, newType, newAantal, newMotor, newKleur, newGebouw, newGebouw2, newParking, newMontage, newVliegraam, newBallustrade, newBevestiging, newLadders, newLaddersHoogte, newVerdiep, newKabel, newKabelafwerking, newAutomatisatie, newBereikbaarheid, newOpmerking]);

  useEffect(() => {
    localStorage.setItem("Foto", JSON.stringify(newFoto));
  }, [newFoto]);

  //Aantal producten
  const [productenSize, setProductenSize] = useState("");

  //Image states
  const [file, setFile] = useState("");
  const [url, setUrl] = useState("");
  const [isProgress, setProgress] = useState(0);
  const [disable, setDisable] = useState(true);
  const [disable2, setDisable2] = useState(true);

  //Id van aangemaakte project
  const [docRefId, setDocRefId] = useState();
  const [proRefId, setProRefId] = useState();

  //Project (informatie) toevoegen aan db
  const verzendenInformatie = async () => {
    const ProjectenCollection = collection(db, "projecten");

    if (newProjectnaam == "" || 
        newDatum == "" || 
        newVoornaam == "" || 
        newAchternaam == "" || 
        newAdres == "" || 
        newGemeente == "" || 
        newTelefoon == "" || 
        newEmail == "" || 
        newBtw == "" ||
        newProjectnaam.length < 3 ||
        newDatum.length < 8 ||
        newVoornaam.length < 3 ||
        newAchternaam.length < 3 ||
        newAdres.length < 3 ||
        newGemeente.length < 3 ||
        newTelefoon.length < 10 ||
        newEmail.length < 7) {
      alert('Sommige velden zijn leeg of bevatten een foutmelding!');
      return;
    }
    //1ste letter een hoofdletter maken
    let hoofdletterP = newProjectnaam.charAt(0).toUpperCase();
    let projectnaam = hoofdletterP + newProjectnaam.slice(1);

    let hoofdletterV = newVoornaam.charAt(0).toUpperCase();
    let voornaam = hoofdletterV + newVoornaam.slice(1);

    let hoofdletterA = newAchternaam.charAt(0).toUpperCase();
    let achternaam = hoofdletterA + newAchternaam.slice(1);

    let hoofdletterAD = newAdres.charAt(0).toUpperCase();
    let adres = hoofdletterAD + newAdres.slice(1);

    let hoofdletterG = newGemeente.charAt(0).toUpperCase();
    let gemeente = hoofdletterG + newGemeente.slice(1);

    await addDoc(ProjectenCollection, {
      project: projectnaam,
      datum: newDatum,
      voornaam: voornaam,
      achternaam: achternaam,
      adres: adres,
      gemeente: gemeente,
      telefoon: newTelefoon,
      email: newEmail,
      btw: newBtw,
      bookmark: "false",
      status: "niet afgewerkt"
    })
    .then((docRef) => {
      //id van project bijhouden
      setDocRefId(docRef.id);
      //inputvelden leegmaken
      let projectNaam = document.getElementById('projectNaam').lastChild.lastChild.lastChild.firstChild;
      projectNaam.value = "";
      let projectDatum = document.getElementById('projectDatum').lastChild.lastChild.lastChild.firstChild;
      projectDatum.value = "";
      let klantVoornaam = document.getElementById('klantVoornaam').lastChild.lastChild.lastChild.firstChild;
      klantVoornaam.value = "";
      let klantAchternaam = document.getElementById('klantAchternaam').lastChild.lastChild.lastChild.firstChild;
      klantAchternaam.value = "";
      let klantAdres = document.getElementById('klantAdres').lastChild.lastChild.lastChild.firstChild;
      klantAdres.value = "";
      let klantGemeente = document.getElementById('klantGemeente').lastChild.lastChild.lastChild.firstChild;
      klantGemeente.value = "";
      let klantEmail = document.getElementById('klantEmail').lastChild.lastChild.lastChild.firstChild;
      klantEmail.value = "";
      let klantTelefoon = document.getElementById('klantTelefoon').lastChild.lastChild.lastChild.firstChild;
      klantTelefoon.value = "";
      let klantBtw = document.getElementById('klantBtw').lastChild.lastChild.lastChild.childNodes[1];
      klantBtw.innerText = "";
      //Wilt niet werken dus moesten we het op bovenste manier doen
      /*setNewProjectnaam("");
      setNewDatum("");
      setNewVoornaam("");
      setNewAchternaam("");
      setNewAdres("");
      setNewGemeente("");
      setNewTelefoon("");
      setNewEmail("");
      setNewBtw("");
      setNewProduct("");
      setNewType("");
      setNewAantal("");
      setNewMotor("");
      SetNewKleur("");
      setNewGebouw("");
      setNewGebouw2("");
      setNewMontage("");
      setNewVliegraam("");
      setNewBallustrade("");
      setNewBevestiging("");
      setNewLadders("");
      setNewLaddersHoogte("");
      setNewVerdiep("");
      setNewKabel("");
      setNewKabelafwerking("");
      setNewAutomatisatie("");
      setNewBereikbaarheid("");
      setNewOpmerking("");*/
      //localStorage leegmaken
      localStorage.clear();
      //producten count terug op 1 zetten
      setProductenSize(1);
      //melding
      alert('Project aangemaakt. Voeg 1 of meerdere producten toe.');
      //popup product openen
      setPopupOpenedSecond(true);
    })
    .catch((e) => {
      alert('Project kan niet worden aangemaakt.');
      return;
    });
  };

  //Product(en) toevoegen aan het project
  const addProductForProject = () => {

    const projectenDocRef = collection(db, "projecten", docRefId, "producten");
    addDoc(projectenDocRef, {
      productNaam: newProduct,
      type: newType,
      aantal: newAantal,
      motor: newMotor,
      kleur: newKleur,
      gebouw: newGebouw,
      gebouw2: newGebouw2,
      parking: newParking,
      montage: newMontage,
      vliegraam: newVliegraam,
      newBallustrade: newBallustrade,
      bevestiging: newBevestiging,
      ladders: newLadders,
      newLaddersHoogte: newLaddersHoogte,
      verdiep: newVerdiep,
      newKabel: newKabel,
      kabelafwerking: newKabelafwerking,
      automatisatie: newAutomatisatie,
      bereikbaarheid: newBereikbaarheid,
      opmerking: newOpmerking,
      aanwezigheid: true
      
    }).then((docId) => {
      //aantal producten updaten
      onSnapshot(projectenDocRef, (doc) => {
        setProductenSize(Number(doc.size) + 1);
      });
      alert('Product ' + (productenSize) + ' is toegevoegd aan het project. Voeg 1 of meerdere afbeeldingen toe.');
      //id van product bijhouden
      setProRefId(docId.id);
      //inputvelden leegmaken
      let productNaam = document.getElementById('productNaam').lastChild.lastChild.lastChild.childNodes[1];
      productNaam.innerText = "";
      let productType = document.getElementById('productType').lastChild.lastChild.lastChild.firstChild;
      productType.value = "";
      let productAantal = document.getElementById('productAantal').lastChild.lastChild.lastChild.firstChild;
      productAantal.value = "";
      let productMotor = document.getElementById('productMotor').lastChild.lastChild.lastChild.childNodes[1];
      productMotor.innerText = "";
      let productKleur = document.getElementById('productKleur').lastChild.lastChild.lastChild.firstChild;
      productKleur.value = "";
      let productGebouw = document.getElementById('productGebouw').lastChild.lastChild.lastChild.childNodes[1];
      productGebouw.innerText = "";
      let productGebouw2 = document.getElementById('productGebouw2').lastChild.lastChild.lastChild.childNodes[1];
      productGebouw2.innerText = "";
      let productParking = document.getElementById('productParking').lastChild.lastChild.lastChild.childNodes[1];
      productParking.innerText = "";
      let productMontage = document.getElementById('productMontage').lastChild.lastChild.lastChild.childNodes[1];
      productMontage.innerText = "";
      let productVliegenraam = document.getElementById('productVliegenraam').lastChild.lastChild.lastChild.childNodes[1];
      productVliegenraam.innerText = "";
      let productBallustrade = document.getElementById('productBallustrade').lastChild.lastChild.lastChild.childNodes[1];
      productBallustrade.innerText = "";
      let productBevestiging = document.getElementById('productBevestiging').lastChild.lastChild.lastChild.childNodes[1];
      productBevestiging.innerText = "";
      let productLadders = document.getElementById('productLadders').lastChild.lastChild.lastChild.childNodes[1];
      productLadders.innerText = "";
      let productHoogteLadders = document.getElementById('productHoogteLadders').lastChild.lastChild.lastChild.firstChild;
      productHoogteLadders.value = "";
      let productVerdiep = document.getElementById('productVerdiep').lastChild.lastChild.lastChild.childNodes[1];
      productVerdiep.innerText = "";
      let productKabel = document.getElementById('productKabel').lastChild.lastChild.lastChild.childNodes[1];
      productKabel.innerText = "";
      let productKabelafwerking = document.getElementById('productKabelafwerking').lastChild.lastChild.lastChild.childNodes[1];
      productKabelafwerking.innerText = "";
      let productAutomatisatie = document.getElementById('productAutomatisatie').lastChild.lastChild.lastChild.childNodes[1];
      productAutomatisatie.innerText = "";
      let productBereikbaarheid = document.getElementById('productBereikbaarheid').lastChild.lastChild.lastChild.childNodes[1];
      productBereikbaarheid.innerText = "";
      let productOpmerkingen = document.getElementById('productOpmerkingen').lastChild.lastChild.lastChild.firstChild;
      productOpmerkingen.value = "";
      //Wilt niet werken dus moesten we het op bovenste manier doen
      /*setNewProduct("");
      setNewType("");
      setNewAantal("");
      setNewMotor("");
      SetNewKleur("");
      setNewGebouw("");
      setNewGebouw2("");
      setNewMontage("");
      setNewVliegraam("");
      setNewBallustrade("");
      setNewBevestiging("");
      setNewLadders("");
      setNewLaddersHoogte("");
      setNewVerdiep("");
      setNewKabel("");
      setNewKabelafwerking("");
      setNewAutomatisatie("");
      setNewBereikbaarheid("");
      setNewOpmerking("");*/
      //localStorage leegmaken
      localStorage.clear();
    }).catch((e) => {
      alert('Product kan niet toegevoegd worden aan het project.');
      return;
    })
  }

  const addPhotoForProduct = () => {

    const projectenDocRef = collection(db, "projecten", docRefId, "producten", proRefId, "foto's");
    addDoc(projectenDocRef, {
      imageUrl: url,
    }).then(() => {
      alert('Afbeelding is toegevoegd aan product ' + Number(productenSize - 1) + '.');
      //button inactief maken
      setDisable2(true);
      setProgress(0);
      //img leegmaken
      setUrl("");
      //localStorage leegmaken
      localStorage.clear();
    }).catch((e) => {
      alert('Afbeelding kan niet toegevoegd worden aan het product.');
      return;
    })
  }

  function handleChange(e) {
    setFile(e.target.files[0]);
    setDisable(false);
  }

  const uploadImage = () => {
    const storageRef = ref(storage, 'Afbeeldingen/' + file.name);
 
    const metadata = {
      contentType: 'image/jpeg',
    };

    //Upload the file and metadata
    const uploadTask = uploadBytesResumable(storageRef, file, metadata);

    uploadTask.on('state_changed', (snapshot) => {
      //Observe state change events such as progress, pause, and resume. Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
      const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      setProgress(progress)
      switch (snapshot.state) {
        case 'paused':
          console.log('Upload is paused');
          break;
        case 'running':
          console.log('Upload is running');
          break;
      }
    },
    (error) => {
      alert('Kan foto niet uploaden.');
    },
    () => {
      //Handle successful uploads on complete. For instance, get the download URL: https://firebasestorage.googleapis.com/...
      getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
        setUrl(downloadURL);
        setNewFoto(downloadURL);
        setDisable(true);
        setDisable2(false);
      });
    });
  }
  
 
  const [popupOpenedSecond, setPopupOpenedSecond] = useState(false);
  const [popupOpenedTirth, setPopupOpenedTirth] = useState(false);
  
  //https://framework7.io/react/inputs.html
  return (
    <List noHairlinesMd>
      {/* Inputvelden tonen informatie */}

      <ListInput
        id="projectNaam"
        label="Project naam:"
        defaultValue={localProjectnaam}
        onBlur={(event) => {setNewProjectnaam(event.target.value)}}
        type="text"
        minlength={3}
        maxlength={50}
        required
        validateOnBlur
        pattern="[a-zA-Zéèçàùµ .'-]*"
        errorMessage="Geen getallen of vreemde tekens aub!"
        placeholder="Naam van het project"
        onInputClear={() => {setNewProjectnaam("")}}
        clearButton>
      </ListInput>

      <ListInput
        id="projectDatum"
        label="Datum opmeting:"
        defaultValue={localProjectDatum}
        type="text"
        minlength={8}
        maxlength={50}
        pattern="[0-9/]*"
        errorMessage="Datum heeft geen correcte stijl!"
        required
        onInputClear={() => {setNewDatum("")}}
        placeholder='Datum van opmeting'
        validate
        onChange={(event) => {setNewDatum(event.target.value)}}
        clearButton>
      </ListInput>

      <ListInput
        id="klantVoornaam"
        label="Voornaam klant:"
        defaultValue={localKlantVoornaam}
        onBlur={(event) => {setNewVoornaam(event.target.value)}}
        type="text"
        onInputClear={() => {setNewVoornaam("")}}
        errorMessage="Geen getallen of vreemde tekens aub!"
        required
        minlength={3}
        maxlength={50}
        validateOnBlur
        pattern="[a-zA-Zéèçàùµ .'-]*"
        placeholder="Voornaam van de klant"
        clearButton>
      </ListInput>

      <ListInput
        id="klantAchternaam"
        label="Achternaam klant:"
        defaultValue={localKlantAchternaam}
        onBlur={(event) => {setNewAchternaam(event.target.value)}}
        type="text"
        errorMessage="Geen getallen of vreemde tekens aub!"
        required
        minlength={3}
        maxlength={50}
        validateOnBlur
        onInputClear={() => {setNewAchternaam("")}}
        pattern="[a-zA-Zéèçàùµ .'-]*"
        placeholder="Achternaam van de klant"
        clearButton>
      </ListInput>

      <ListInput
        id="klantAdres"
        label="Adres:"
        defaultValue={localKlantAdres}
        onBlur={(event) => {setNewAdres(event.target.value)}}
        type="text"
        errorMessage="Geen vreemde tekens aub!"
        required
        validateOnBlur
        minlength={3}
        maxlength={150}
        onInputClear={() => {setNewAdres("")}}
        pattern="[a-zA-Z0-9éèçàùµ, .'-]*"
        placeholder="Adres van de klant"
        clearButton>
      </ListInput>

      <ListInput
        id="klantGemeente"
        label="Gemeente:"
        defaultValue={localKlantGemeente}
        onBlur={(event) => {setNewGemeente(event.target.value)}}
        type="text"
        errorMessage="Geen vreemde tekens aub!"
        required
        onInputClear={() => {setNewGemeente("")}}
        validateOnBlur
        minlength={3}
        maxlength={50}
        pattern="[a-zA-Z0-9éèçàùµ, .'-]*"
        placeholder="Gemeente van de klant"
        clearButton>
      </ListInput>

      <ListInput
        id="klantTelefoon"
        label="Telefoon:"
        defaultValue={localKlantTelefoon}
        onBlur={(event) => {setNewTelefoon(event.target.value)}}
        type="tel"
        errorMessage="Voer een geldig telefoonnummer in aub!"
        required
        minlength={10}
        maxlength={12}
        validateOnBlur
        pattern="[0-9+]*"
        onInputClear={() => {setNewTelefoon("")}}
        placeholder="Telefoonnummer van de klant"
        clearButton>
      </ListInput>

      <ListInput
        id="klantEmail"
        label="E-mail:"
        defaultValue={localKlantEmail}
        onBlur={(event) => {setNewEmail(event.target.value)}}
        type="email"
        errorMessage="Voer een geldig email in aub!"
        required
        maxlength={50}
        validateOnBlur
        onInputClear={() => {setNewEmail("")}}
        pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
        placeholder="Email van de klant"
        clearButton>
      </ListInput>

      <ListItem id="klantBtw" title="BTW" smartSelect smartSelectParams={{ openIn: 'sheet' }} required>
        <select onChange={(e) => {setNewBtw(e.target.value)}} name="BTW" defaultValue={localProjectBtw}>
          <option value=""></option>
          <option value="0%">0%</option>
          <option value="6%">6%</option>
          <option value="12%">12%</option>
          <option value="21%">21%</option>
        </select>
      </ListItem>

      <Button className="btnVerzendProject"  onClick={verzendenInformatie} large fill>Maak project aan</Button>

      {/* Product popupOpen=".addSecond-popup"*/}
      <Popup
        className="addSecond-popup"
        opened={popupOpenedSecond}
        onPopupClosed={() => setPopupOpenedSecond(false)}
        tabletFullscreen>
        <Page>
          <Navbar title="Aanmaken Nieuw Product">
            <Badge color="red">{productenSize}</Badge>
            <NavRight>
              <Link popupClose>Sluit</Link>
            </NavRight>
          </Navbar>
          <Block>
            <List noHairlinesMd>
              {/* Inputvelden tonen producten */}

              <ListItem id="productNaam" title="Product" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewProduct(e.target.value)}} name="Product" defaultValue={localProjectProduct}>
                  <option value=""></option>
                  <option value="Rolluik">Rolluik</option>
                  <option value="Screen(zip)">Screen(zip)</option>
                  <option value="Zonnetent">Zonnetent</option>
                  <option value="Uitvalscherm">Uitvalscherm</option>
                  <option value="Binnenzonwering">Binnenzonwering</option>
                  <option value="Vliegenraam">Vliegenraam</option>
                  <option value="Vliegenschuifdeur">Vliegenschuifdeur</option>
                </select>
              </ListItem>

              <ListInput
                id="productType"
                label="Type:"
                defaultValue={localProjectType}
                onBlur={(event) => {setNewType(event.target.value)}}
                type="textarea"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <ListInput
                id="productAantal"
                label="Aantal:"
                defaultValue={localProjectAantal}
                onBlur={(event) => {setNewAantal(event.target.value)}}
                type="number"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <ListItem id="productMotor" title="Motor" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewMotor(e.target.value)}} name="Motor" defaultValue={localProjectMotor}>
                  <option value=""></option>
                  <option value="Somfy LT">Somfy LT</option>
                  <option value="Somfy IO">Somfy IO</option>
                  <option value="Somfy RTS">Somfy RTS</option>
                  <option value="Zaffer">Zaffer</option>
                  <option value="Zaffer AIO">Zaffer AIO</option>
                  <option value="Solar Somfy">Solar Somfy</option>
                  <option value="Solar GLR">Solar GLR</option>
                </select>
              </ListItem>

              <ListInput
                id="productKleur"
                label="Kleur:"
                defaultValue={localProjectKleur}
                onBlur={(event) => {SetNewKleur(event.target.value)}}
                type="textarea"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <ListItem id="productGebouw" title="Gebouw" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewGebouw(e.target.value)}} name="Gebouw" defaultValue={localProjectGebouw}>
                  <option value=""></option>
                  <option value="Huis">Huis</option>
                  <option value="Klein gebouw">Klein gebouw</option>
                  <option value="Groot gebouw">Groot gebouw</option>
                  <option value="Hoog gebouw">Hoog gebouw</option>
                </select>
              </ListItem>

              <ListItem id="productGebouw2" title="Gebouw 2" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewGebouw2(e.target.value)}} name="Gebouw 2" defaultValue={localProjectGebouw2}>
                  <option value=""></option>
                  <option value="Bestaand">Bestaand</option>
                  <option value="Nieuwbouw">Nieuwbouw</option>
                  <option value="Renovatie">Renovatie</option>
                </select>
              </ListItem>

              <ListItem id="productParking" title="Parking" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewParking(e.target.value)}} name="Parking" defaultValue={localProjectParking}>
                  <option value=""></option>
                  <option value="Makkelijk">Makkelijk</option>
                  <option value="Moeilijk">Moeilijk</option>
                  <option value="Parking">Parking</option>
                </select>
              </ListItem>

              <ListItem id="productMontage" title="Montage" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewMontage(e.target.value)}} name="Montage" defaultValue={localProjectMontage}>
                  <option value=""></option>
                  <option value="In de dag">In de dag</option>
                  <option value="Op de dag">Op de dag</option>
                  <option value="Nis">Nis</option>
                  <option value="Op chassis">Op chassis</option>
                </select>
              </ListItem>

              <ListItem id="productVliegenraam" title="Vliegenraam aanwezig" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewVliegraam(e.target.value)}} name="Vliegenraam aanwezig" defaultValue={localProjectVliegraam}>
                  <option value=""></option>
                  <option value="Nee">Nee</option>
                  <option value="Ja">Ja</option>
                </select>
              </ListItem>

              <ListItem id="productBallustrade" title="Ballustrade aanwezig" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewBallustrade(e.target.value)}} name="Ballustrade aanwezig" defaultValue={localProjectBallustrade}>
                  <option value=""></option>
                  <option value="Nee">Nee</option>
                  <option value="Ja">Ja</option>
                </select>
              </ListItem>

              <ListItem id="productBevestiging" title="Bevestiging in" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewBevestiging(e.target.value)}} name="Bevestiging in" defaultValue={localProjectBevestinging}>
                  <option value=""></option>
                  <option value="Hout">Hout</option>
                  <option value="Steen">Steen</option>
                  <option value="Alu">Alu</option>
                  <option value="Staal">Staal</option>
                  <option value="Inox">Inox</option>
                  <option value="Gyproc">Gyproc</option>
                  <option value="Crepi">Crepi</option>
                </select>
              </ListItem>

              <ListItem id="productLadders" title="Ladders" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewLadders(e.target.value)}} name="Ladders" defaultValue={localProjectLadders}>
                  <option value=""></option>
                  <option value="Schuifladder">Schuifladder</option>
                  <option value="Normale ladders">Normale ladders</option>
                  <option value="Stelling">Stelling</option>
                  <option value="Hoogwerker">Hoogwerker</option>
                </select>
              </ListItem>

              <ListInput
                id="productHoogteLadders"
                label="Hoogte ladders:"
                defaultValue={localProjectLadders}
                onBlur={(event) => {setNewLaddersHoogte(event.target.value)}}
                type="textarea"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <ListItem id="productVerdiep" title="Verdiep" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewVerdiep(e.target.value)}} name="Verdiep" defaultValue={localProjectVerdiep}>
                  <option value=""></option>
                  <option value="Gelijksvloer">Gelijksvloer</option>
                  <option value="1ste verdiep">1ste verdiep</option>
                  <option value="2de verdiep">2de verdiep</option>
                  <option value="3de verdiep">3de verdiep</option>
                </select>
              </ListItem>

              <ListItem id="productKabel" title="Kabel" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewKabel(e.target.value)}} name="Kabel" defaultValue={localProjectKabel}>
                  <option value=""></option>
                  <option value="Muurdoorboring">Muurdoorboring</option>
                  <option value="Door chassis">Door chassis</option>
                  <option value="Voorzien door klant (kabel of buis)">Voorzien door klant (kabel of buis)</option>
                  <option value="Via geleider">Via geleider</option>
                  <option value="Op de kast">Op de kast</option>
                </select>
              </ListItem>

              <ListItem id="productKabelafwerking" title="Kabelafwerking" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewKabelafwerking(e.target.value)}} name="Kabelafwerking" defaultValue={localProjectKabelafwerking}>
                  <option value=""></option>
                  <option value="Kabelgoot (kleur)">Kabelgoot (kleur)</option>
                  <option value="Buis">Buis</option>
                  <option value="Slijpen in muur">Slijpen in muur</option>
                  <option value="Voorzien door klant">Voorzien door klant</option>
                </select>
              </ListItem>

              <ListItem id="productAutomatisatie" title="Automatisatie" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewAutomatisatie(e.target.value)}} name="Automatisatie" defaultValue={localProjectAutomatisatie}>
                  <option value=""></option>
                  <option value="3D wind">3D wind</option>
                  <option value="Zonnecel sunis wirefree IO">Zonnecel sunis wirefree IO</option>
                  <option value="Wind wire free IO">Wind wire free IO</option>
                  <option value="Soliris sensor RTS">Soliris sensor RTS</option>
                  <option value="Eolis sensor RTS">Eolis sensor RTS</option>
                  <option value="Situo 5 IO">Situo 5 IO</option>
                  <option value="Situo 5 variation">Situo 5 variation</option>
                  <option value="Smoove IO">Smoove IO</option>
                  <option value="Telis 1">Telis 1</option>
                  <option value="Telis 4">Telis 4</option>
                  <option value="Smoove RTS">Smoove RTS</option>
                  <option value="Zaffer zender">Zaffer zender</option>
                  <option value="Zon/wind">Zon/wind</option>
                </select>
              </ListItem>

              <ListItem id="productBereikbaarheid" title="Bereikbaarheid" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewBereikbaarheid(e.target.value)}} name="Bereikbaarheid" defaultValue={localProjectBereikbaarheid}>
                  <option value=""></option>
                  <option value="Lift">Lift</option>
                  <option value="Verhuislift">Verhuislift</option>
                  <option value="Gelijksvloer">Gelijksvloer</option>
                  <option value="Gelijksvloer: opgepast met lengte">Gelijksvloer: opgepast met lengte</option>
                </select>
              </ListItem>

              <ListInput
                id="productOpmerkingen"
                label="Opmerkingen:"
                defaultValue={localProjectOpmerkingen}
                onBlur={(event) => {setNewOpmerking(event.target.value)}}
                type="textarea"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <Button className='btnVerzendProject' onClick={addProductForProject} popupOpen=".addTirth-popup" large fill>Voeg Afbeeldingen Toe</Button>
            </List>
          </Block>
        </Page>
      </Popup>

      {/* Foto's */}
      <Popup
        className="addTirth-popup"
        opened={popupOpenedTirth}
        onPopupClosed={() => setPopupOpenedTirth(false)}
        tabletFullscreen>
        <Page>
          <Navbar title="Toevoegen Van Afbeeldingen">
            <NavRight>
              <Link popupClose>Sluit</Link>
            </NavRight>
          </Navbar>
         
          <Block>
            <Input type="file" accept="image/*" capture="camera" onChange={handleChange} className="inputtypeImage"></Input>
            <Button onClick={uploadImage} disabled={disable}>Upload foto</Button>
            <p>{isProgress} %</p>
            <Progressbar color="blue" progress={isProgress}/>
            <div>
              <img src={url} className="imgPost"></img>
            </div>
          </Block>

          <Button className='btnVerzendProject' onClick={addPhotoForProduct} disabled={disable2} large fill>Afbeelding Toevoegen</Button>
          {/* <Button className="btnVerzendProject" onClick={popupOpenedTirth} color="green" large fill>Nieuw Product</Button> */}
        </Page>
      </Popup>
    </List>
  )
};
export default FirstAddProjectModal;
