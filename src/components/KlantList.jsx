import React, { useState, useEffect } from 'react';
import {
  SwipeoutActions,
  SwipeoutButton,
  List,
  ListItem
} from 'framework7-react';
import { initializeApp } from 'firebase/app';
import { getFirestore, collection, doc, onSnapshot, query,orderBy } from 'firebase/firestore';


//Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDqcQyhGE5MXMS0bTb9y-_NgEitwRYf_r8",
  authDomain: "shadowmaker-82b93.firebaseapp.com",
  projectId: "shadowmaker-82b93",
  storageBucket: "shadowmaker-82b93.appspot.com",
  messagingSenderId: "302524693982",
  appId: "1:302524693982:web:5ca9e54699821dcdb4714f",
  measurementId: "G-V9B33GZR83"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

const KlantList = () => {

    const [klanten, setKlanten] = useState([]);
  

    //Klanten ophalen
    useEffect(() => {
        const getKlanten = async () => {
        const q = query(collection(db, "projecten"), orderBy("achternaam", "asc"));
        onSnapshot(q, (doc) => {
            setKlanten(doc.docs.map((doc) => ({...doc.data(), id: doc.id})));
        });
        };
        getKlanten();
    },[]);

    //Locatie zoeken --- https://www.pluralsight.com/guides/how-to-use-geolocation-call-in-reactjs
    const getLocation = (id) => {
        const q = query(doc(db, "projecten", id));
        onSnapshot(q, (doc) => {  // where data.achternaam == (project) achternaam en die data kan je in project info zetten voor klant
            const data = doc.data();
                navigator.geolocation.getCurrentPosition(
                function(position) {
                    //latitude en longitude
                    //console.log(position);
                    //console.log("Latitude is :", position.coords.latitude);
                    //console.log("Longitude is :", position.coords.longitude);
                    //window.open("https://maps.google.com?q="+position.coords.latitude+","+position.coords.longitude);

                    //adres
                    window.open("https://maps.google.com?q="+ data.adres + ", " + data.gemeente);
                },
                function(error) {
                    //console.error("Error Code = " + error.code + " - " + error.message);
                    alert('Google maps kan niet geopend worden. Zet je locatie aan.');
                }
            );
        });
    };

    //klanten tonen met de knop verwijderen
    return (
        <List id="listKlanten" className="search-list searchbar-found" sortable>
            {klanten.map((klant) => {
                return <ListItem key={klant.id} header="" title={klant.achternaam + " " + klant.voornaam} swipeout routeProps={klant} after={klant.telefoon} link="/details-klant/:id/">
                    <SwipeoutActions right>
                        <SwipeoutButton color='blue' onClick={() => getLocation(klant.id)} confirmText="Ben je zeker naar google maps te gaan?" confirmTitle="Route klant">Route</SwipeoutButton>
                    </SwipeoutActions>
                </ListItem>
            })}
        </List>
    )
}
export default KlantList;