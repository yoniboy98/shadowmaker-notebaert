import React from 'react';
import {
  Icon,
  Block,
  Link
} from 'framework7-react';

const AanmakenProjectIcon = () => {
  return ( 
    <Block>  
      <Link fill className="projectAanmakenIconBlock" popupOpen=".addFirst-popup" iconOnly={true}>
        <Icon f7="plus_rectangle_on_rectangle" size="300px"></Icon>
      </Link>
      <p className="projectAanmakenText">Een nieuw project aanmaken</p>
    </Block>    
  )
};
export default AanmakenProjectIcon;