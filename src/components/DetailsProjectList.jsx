import React, { useState, useEffect, useRef } from 'react';
import {
  List,
  ListItem,
  Page,
  Block,
  NavRight,
  Toolbar,
  Tab,
  Badge,
  Tabs,
  Input,
  Progressbar,
  ListInput,
  Button,
  SwipeoutButton,
  SwipeoutActions,
  Navbar,
  Link,
  Popup,
  f7,
  PhotoBrowser,
  Icon
} from 'framework7-react';
import { initializeApp } from 'firebase/app';
import { getFirestore, onSnapshot, doc, updateDoc, query, collection, addDoc, deleteDoc, orderBy} from 'firebase/firestore';
import { getStorage, ref, uploadBytesResumable, getDownloadURL} from "firebase/storage";

//Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDqcQyhGE5MXMS0bTb9y-_NgEitwRYf_r8",
  authDomain: "shadowmaker-82b93.firebaseapp.com",
  projectId: "shadowmaker-82b93",
  storageBucket: "shadowmaker-82b93.appspot.com",
  messagingSenderId: "302524693982",
  appId: "1:302524693982:web:5ca9e54699821dcdb4714f",
  measurementId: "G-V9B33GZR83"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const storage = getStorage(app);

const DetailsProjectList = (props) => {

  //useState
  const [popupOpened, setPopupOpened] = useState(false);
  const [popupOpened2, setPopupOpened2] = useState(false);
  const [popupOpened3, setPopupOpened3] = useState(false);
  const [popupOpened4, setPopupOpened4] = useState(false);
  const [popupOpened5, setPopupOpened5] = useState(false);
  const [popupOpened6, setPopupOpened6] = useState(false);

  const [details, setDetails] = useState([]);

  const [newProjectnaam, setNewProjectnaam] = useState("");
  const [newDatum, setNewDatum] = useState("");
  const [newVoornaam, setNewVoornaam] = useState("");
  const [newAchternaam, setNewAchternaam] = useState("");
  const [newAdres, setNewAdres] = useState("");
  const [newGemeente, setNewGemeente] = useState("");
  const [newTelefoon, setNewTelefoon] = useState("");
  const [newEmail, setNewEmail] = useState("");
  const [newBtw, setNewBtw] = useState("");

  const [newProduct, setNewProduct] = useState("");
  const [newType, setNewType] = useState("");
  const [newAantal, setNewAantal] = useState("");
  const [newMotor, setNewMotor] = useState("");
  const [newKleur, SetNewKleur] = useState("");
  const [newGebouw, setNewGebouw] = useState("");
  const [newGebouw2, setNewGebouw2] = useState("");
  const [newParking, setNewParking] = useState("");
  const [newMontage, setNewMontage] = useState("");
  const [newVliegraam, setNewVliegraam] = useState("");
  const [newBallustrade, setNewBallustrade] = useState("");
  const [newBevestiging, setNewBevestiging] = useState("");
  const [newLadders, setNewLadders] = useState("");
  const [newLaddersHoogte, setNewLaddersHoogte] = useState("");
  const [newVerdiep, setNewVerdiep] = useState("");
  const [newKabel, setNewKabel] = useState("");
  const [newKabelafwerking, setNewKabelafwerking] = useState("");
  const [newAutomatisatie, setNewAutomatisatie] = useState("");
  const [newBereikbaarheid, setNewBereikbaarheid] = useState("");
  const [newOpmerking, setNewOpmerking] = useState("");

  //Project updaten
  const UpdateProject = async (id) => {
    const projectReference = doc(db, "projecten", id);
    await updateDoc(projectReference, {
      project: newProjectnaam || details[0],
      datum: newDatum || details[1],
      voornaam: newVoornaam || details[2],
      achternaam: newAchternaam || details[3],
      adres: newAdres || details[4],
      gemeente: newGemeente || details[5],
      telefoon: newTelefoon || details[6],
      email: newEmail || details[7],
      btw: newBtw || details[8]
    }).then(() => {
      alert('Het project is succesvol aangepast.');
    })
    .catch(() => {
      alert('Het is niet gelukt om het project te bewerken.');
    });
  }

  //Product updaten
  const UpdateProduct = async (id) => {
    const productReference = doc(db, "projecten", props.id, "producten", id);
    await updateDoc(productReference, {
      productNaam: newProduct || detailsOfInputProduct[19],
      type: newType || detailsOfInputProduct[0],
      aantal: newAantal || detailsOfInputProduct[1],
      motor: newMotor || detailsOfInputProduct[2],
      kleur: newKleur || detailsOfInputProduct[3],
      gebouw: newGebouw || detailsOfInputProduct[4],
      gebouw2: newGebouw2 || detailsOfInputProduct[5],
      parking: newParking || detailsOfInputProduct[6],
      montage: newMontage || detailsOfInputProduct[7],
      vliegraam: newVliegraam || detailsOfInputProduct[8],
      newBallustrade: newBallustrade || detailsOfInputProduct[9],
      bevestiging: newBevestiging || detailsOfInputProduct[10],
      ladders: newLadders || detailsOfInputProduct[11],
      newLaddersHoogte: newLaddersHoogte || detailsOfInputProduct[12],
      verdiep: newVerdiep || detailsOfInputProduct[13],
      newKabel: newKabel || detailsOfInputProduct[14],
      kabelafwerking: newKabelafwerking || detailsOfInputProduct[15],
      automatisatie: newAutomatisatie || detailsOfInputProduct[16],
      bereikbaarheid: newBereikbaarheid || detailsOfInputProduct[17],
      opmerking: newOpmerking || detailsOfInputProduct[18]
    }).then(() => {
      alert('Het product is succesvol aangepast.');
    })
    .catch((error) => {
      alert('Het is niet gelukt om het product te bewerken.');
    });
  }

  const [sizeDetailsOfProducts, setSizeDetailsOfProducts] = useState();

  const getDetailsProject = async () => {
    onSnapshot(doc(db, "projecten", props.id), (doc) => {
      const data = doc.data();
      setDetails([
        data.project, 
        data.datum,
        data.voornaam,
        data.achternaam,
        data.adres,
        data.gemeente,
        data.telefoon,
        data.email,
        data.btw
      ]);
    });
  }

  const getDetailsProducten = async () => {
    const q = query(collection(db, "projecten", props.id, "producten"), orderBy("productNaam", "asc"));
    onSnapshot(q, (doc) => { 
      setDetailsOfProducts(doc.docs.map((doc) => ({...doc.data(), id: doc.id})));
      setSizeDetailsOfProducts(doc.size);
    });
  }

  useEffect(() => {
    getDetailsProject();
    getDetailsProducten();
  },[]);

  const [images, setImages] = useState([]);
  
  const [productId, setProductId] = useState();

  const getIdOfProduct = async (id)  => {

    setProductId(id);
    const q = query(collection(db, "projecten", props.id, "producten", id, "foto's"));
    let photos = [];

    onSnapshot(q, (querySnapshot) => {
      querySnapshot.forEach((doc) => {
        photos.push(doc.data().imageUrl);
      });
      setImages(photos);
    });

    onSnapshot(doc(db, "projecten", props.id, "producten", id), (doc) => {
      const data = doc.data();
      setDetailsOfInputProduct([
        data.type,
        data.aantal,
        data.motor,
        data.kleur,
        data.gebouw,
        data.gebouw2,
        data.parking, 
        data.montage, 
        data.vliegraam,
        data.newBallustrade,
        data.bevestiging,
        data.ladders,
        data.newLaddersHoogte,
        data.verdiep,
        data.newKabel,
        data.kabelafwerking,
        data.automatisatie,
        data.bereikbaarheid,
        data.opmerking,
        data.productNaam,
      ]);
    });
  }

  const getAfbeeldingen = () => {
    if (images.length != 0) {
      return <div className='container'>
        <img className="ProductImage" src={images == null ? "Geen afbeeldingen aanwezig" : images} onClick={() => standalone.current.open()} width="100%" height="100%"/>
        <div className="centerText" onClick={() => standalone.current.open()}>Klik om afbeelding te bekijken</div>
      </div>
    }
    else {
      return <div className='container'>
        <img className="ProductImage"/>
        <div className="centerText">Geen afbeeldingen aanwezig.</div>
      </div>
    }
  }

  const [detailsOfProducts, setDetailsOfProducts] = useState([])

  const [detailsOfInputProduct, setDetailsOfInputProduct] = useState([])

  const standalone = useRef(null);

  const [disable, setDisable] = useState(true);
  const [file, setFile] = useState("");
  const [url, setUrl] = useState("");
  const [isProgress, setProgress] = useState(0);
  const [disable2, setDisable2] = useState(true);

  function handleChange(e) {
    setFile(e.target.files[0]);
    setDisable(false);
  }

  const uploadImage = () => {
    const storageRef = ref(storage, 'Afbeeldingen/' + file.name);
 
    const metadata = {
      contentType: 'image/jpeg',
    };

    // Upload the file and metadata
    const uploadTask = uploadBytesResumable(storageRef, file, metadata);

    uploadTask.on('state_changed', (snapshot) => {
      //Observe state change events such as progress, pause, and resume. Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
      const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      setProgress(progress);
      switch (snapshot.state) {
        case 'paused':
          //console.log('Upload is paused');
          break;
        case 'running':
          //console.log('Upload is running');
          break;
      }
    },
    (error) => {
      alert('Kan foto niet uploaden.');
    },
    () => {
      // Handle successful uploads on complete. For instance, get the download URL: https://firebasestorage.googleapis.com/...
      getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
        setUrl(downloadURL);
        setDisable(true);
        setDisable2(false)
      });
    });
  }

  const addExtraImageForNewProduct = () => {
    const productenDocRef = collection(db, "projecten", props.id, "producten", proRefId, "foto's");

    addDoc(productenDocRef, {
      imageUrl: url,
    }).then(() => {
      alert('Afbeelding is toegevoegd aan het product.');
      //button inactief maken
      setDisable2(true)
      setProgress(0);
      //img leegmaken
      setUrl("");
    }).catch((e) => {
      alert('De afbeelding kan niet toegevoegd worden aan het product.');
    })
  }

  const addExtraImageForProduct = () => {
    const productenDocRef = collection(db, "projecten", props.id, "producten", productId, "foto's");

    addDoc(productenDocRef, {
      imageUrl: url,
    }).then(() => {
      alert('Afbeelding is toegevoegd aan het product.');
      //button inactief maken
      setDisable2(true)
      setProgress(0);
      //img leegmaken
      setUrl("");
    }).catch((e) => {
      alert('De afbeelding kan niet toegevoegd worden aan het product.');
    })
  }

  //Product verwijderen
  const deleteProduct = async (id) => {
    const productReference = doc(db, "projecten", props.id, "producten", id);
    await deleteDoc(productReference)
    .then(() => {
      alert('Het product is verwijdert.');
    })
    .catch((error) => {
      alert('Kan product niet verwijderen.');
    });
  }

  const [proRefId, setProRefId] = useState();

  //Extra product toevoegen
  const addExtraProductForProject = () => {

    const projectenDocRef = collection(db, "projecten", props.id, "producten");
    addDoc(projectenDocRef, {
      productNaam: newProduct,
      type: newType,
      aantal: newAantal,
      motor: newMotor,
      kleur: newKleur,
      gebouw: newGebouw,
      gebouw2: newGebouw2,
      parking: newParking,
      montage: newMontage,
      vliegraam: newVliegraam,
      newBallustrade: newBallustrade,
      bevestiging: newBevestiging,
      ladders: newLadders,
      newLaddersHoogte: newLaddersHoogte,
      verdiep: newVerdiep,
      newKabel: newKabel,
      kabelafwerking: newKabelafwerking,
      automatisatie: newAutomatisatie,
      bereikbaarheid: newBereikbaarheid,
      opmerking: newOpmerking
    }).then((docId) => {
      alert('Product is toegevoegd aan het project.');
      //id van product bijhouden
      setProRefId(docId.id);
      //inputvelden leegmaken
      let productNaam = document.getElementById('productNaam').lastChild.lastChild.lastChild.childNodes[1];
      productNaam.innerText = "";
      let productType = document.getElementById('productType').lastChild.lastChild.lastChild.firstChild;
      productType.value = "";
      let productAantal = document.getElementById('productAantal').lastChild.lastChild.lastChild.firstChild;
      productAantal.value = "";
      let productMotor = document.getElementById('productMotor').lastChild.lastChild.lastChild.childNodes[1];
      productMotor.innerText = "";
      let productKleur = document.getElementById('productKleur').lastChild.lastChild.lastChild.firstChild;
      productKleur.value = "";
      let productGebouw = document.getElementById('productGebouw').lastChild.lastChild.lastChild.childNodes[1];
      productGebouw.innerText = "";
      let productGebouw2 = document.getElementById('productGebouw2').lastChild.lastChild.lastChild.childNodes[1];
      productGebouw2.innerText = "";
      let productParking = document.getElementById('productParking').lastChild.lastChild.lastChild.childNodes[1];
      productParking.innerText = "";
      let productMontage = document.getElementById('productMontage').lastChild.lastChild.lastChild.childNodes[1];
      productMontage.innerText = "";
      let productVliegenraam = document.getElementById('productVliegenraam').lastChild.lastChild.lastChild.childNodes[1];
      productVliegenraam.innerText = "";
      let productBallustrade = document.getElementById('productBallustrade').lastChild.lastChild.lastChild.childNodes[1];
      productBallustrade.innerText = "";
      let productBevestiging = document.getElementById('productBevestiging').lastChild.lastChild.lastChild.childNodes[1];
      productBevestiging.innerText = "";
      let productLadders = document.getElementById('productLadders').lastChild.lastChild.lastChild.childNodes[1];
      productLadders.innerText = "";
      let productHoogteLadders = document.getElementById('productHoogteLadders').lastChild.lastChild.lastChild.firstChild;
      productHoogteLadders.value = "";
      let productVerdiep = document.getElementById('productVerdiep').lastChild.lastChild.lastChild.childNodes[1];
      productVerdiep.innerText = "";
      let productKabel = document.getElementById('productKabel').lastChild.lastChild.lastChild.childNodes[1];
      productKabel.innerText = "";
      let productKabelafwerking = document.getElementById('productKabelafwerking').lastChild.lastChild.lastChild.childNodes[1];
      productKabelafwerking.innerText = "";
      let productAutomatisatie = document.getElementById('productAutomatisatie').lastChild.lastChild.lastChild.childNodes[1];
      productAutomatisatie.innerText = "";
      let productBereikbaarheid = document.getElementById('productBereikbaarheid').lastChild.lastChild.lastChild.childNodes[1];
      productBereikbaarheid.innerText = "";
      let productOpmerkingen = document.getElementById('productOpmerkingen').lastChild.lastChild.lastChild.firstChild;
      productOpmerkingen.value = "";
      //Wilt niet werken dus moesten we het op bovenste manier doen
      /*setNewProduct("");
      setNewType("");
      setNewAantal("");
      setNewMotor("");
      SetNewKleur("");
      setNewGebouw("");
      setNewGebouw2("");
      setNewMontage("");
      setNewVliegraam("");
      setNewBallustrade("");
      setNewBevestiging("");
      setNewLadders("");
      setNewLaddersHoogte("");
      setNewVerdiep("");
      setNewKabel("");
      setNewKabelafwerking("");
      setNewAutomatisatie("");
      setNewBereikbaarheid("");
      setNewOpmerking("");*/
      //localStorage leegmaken
      localStorage.clear();
    }).catch((e) => {
      alert('Product kan niet toegevoegd worden aan het project.');
    })
  }

  //Conditional rendering voor het tonen van het aantal producten
  function DisplaySizeOfProducts () {
    if(sizeDetailsOfProducts === 0) {
      return <Badge color="#2a74e2">Project bevat geen producten.</Badge>
    }
    if (sizeDetailsOfProducts === 1) {
      return <Badge color="#2a74e2">Project bevat {sizeDetailsOfProducts} product.</Badge>
    }
    return <Badge color="#2a74e2">Project bevat {sizeDetailsOfProducts} producten.</Badge>
  }

  return (
    <Page name="Details">
      <Navbar title="Project Details" backLink> 
        <NavRight>
          <Block>
            <Button onClick={() => getDetailsProject()} popupOpen=".demo-popup">
              <Icon f7="pencil_ellipsis_rectangle"></Icon>
            </Button>
          </Block>
        </NavRight>
      </Navbar>

      <Toolbar tabbar top>
        <Link tabLink="#tab-1" tabLinkActive>Gegevens Klant</Link>
        <Link tabLink="#tab-2">Lijst producten</Link>
      </Toolbar>

      {/* Project met details weergeven */}
      <Tabs swipeable className="tabGrid">
        <Tab id="tab-1" className="page-content" tabActive>
          <Block>
            <List noHairlinesMd> {/* <Icon slot="media" icon="demo-list-icon"></Icon> */}
              <ListItem header="Naam project" swipeout title={details[0]}></ListItem>

              <ListItem header="Datum" swipeout title={details[1]}></ListItem>
            
              <ListItem header="Voornaam" swipeout title={details[2]}></ListItem>

              <ListItem header="Achternaam" swipeout title={details[3]}></ListItem>

              <ListItem header="Adres" swipeout title={details[4]}></ListItem>

              <ListItem header="Gemeente" swipeout title={details[5]}></ListItem>

              <ListItem header="Telefoon" swipeout title={details[6]}></ListItem>

              <ListItem header="Email" swipeout title={details[7]}></ListItem>

              <ListItem header="Btw" swipeout title={details[8]}></ListItem>
            </List>
          </Block>
        </Tab>
        <Tab id="tab-2" className="page-content">     
          <div className='addProductIcon'>
            <Link fill popupOpen=".AddExtraProduct-popup" iconOnly={true}>
              <Icon f7="folder_badge_plus" className='toevoegenExtraProduct' color='yellow'></Icon>
            </Link>
          </div>
          <Block>
            {DisplaySizeOfProducts()}

            {detailsOfProducts.map((detail) => {
            return <List noHairlinesMd key={detail.id}>       
              <ListItem header="" routeProps={detail} popupOpen="#details-popup" link title={detail.productNaam} onSwipeoutDelete={() => deleteProduct(detail.id)} swipeout onClick={() => getIdOfProduct(detail.id)}>
                <SwipeoutActions right>
                  <SwipeoutButton delete confirmText="Ben je zeker dat je dit product wilt verwijderen?" confirmTitle="Project verwijderen">Verwijderen</SwipeoutButton>
                </SwipeoutActions>
              </ListItem>
            </List>})}
          </Block>
        </Tab>
      </Tabs>

      {/* Popup om details te bewerken van klant/project */}
      <Popup
        className="demo-popup"
        opened={popupOpened}
        onPopupClosed={() => setPopupOpened(false)}>
        <Page>
          <Navbar title="Project Bewerken">
            <NavRight>
              <Link popupClose>Sluit</Link>
            </NavRight>
          </Navbar>
          <Block>
            <List noHairlinesMd>

              <ListInput
                id="projectNaam"
                label="Project naam:"
                defaultValue={details[0] || newProjectnaam}
                onChange={(event) => {setNewProjectnaam(event.target.value)}}
                type="text"
                minlength={3}
                maxlength={50}
                required
                validate
                pattern="[a-zA-Zéèçàùµ .'-]*"
                errorMessage="Geen getallen of vreemde tekens aub!"
                placeholder="Naam van het project"
                onInputClear={() => {setNewProjectnaam("")}}
                clearButton>
              </ListInput>

              <ListInput
                id="projectDatum"
                label="Datum opmeting:"
                defaultValue={details[1] || newDatum}
                type="text"
                minlength={8}
                maxlength={50}
                pattern="[0-9/]*"
                errorMessage="Datum mag niet leeg zijn!"
                required
                onInputClear={() => {setNewDatum("")}}
                placeholder='Datum van opmeting'
                validate
                onChange={(event) => {setNewDatum(event.target.value)}}
                clearButton>
              </ListInput>

              <ListInput
                id="klantVoornaam"
                label="Voornaam klant:"
                defaultValue={details[2] || newVoornaam}
                onChange={(event) => {setNewVoornaam(event.target.value)}}
                type="text"
                onInputClear={() => {setNewVoornaam("")}}
                errorMessage="Geen getallen of vreemde tekens aub!"
                required
                minlength={3}
                maxlength={50}
                validate
                pattern="[a-zA-Zéèçàùµ .'-]*"
                placeholder="Voornaam van de klant"
                clearButton>
              </ListInput>

              <ListInput
                id="klantAchternaam"
                label="Achternaam klant:"
                defaultValue={details[3] || newVoornaam}
                onChange={(event) => {setNewAchternaam(event.target.value)}}
                type="text"
                errorMessage="Geen getallen of vreemde tekens aub!"
                required
                minlength={3}
                maxlength={50}
                validate
                onInputClear={() => {setNewAchternaam("")}}
                pattern="[a-zA-Zéèçàùµ .'-]*"
                placeholder="Achternaam van de klant"
                clearButton>
              </ListInput>

              <ListInput
                id="klantAdres"
                label="Adres:"
                defaultValue={details[4] || newAdres}
                onChange={(event) => {setNewAdres(event.target.value)}}
                type="text"
                errorMessage="Geen vreemde tekens aub!"
                required
                validate
                minlength={3}
                onInputClear={() => {setNewAdres("")}}
                pattern="[a-zA-Z0-9éèçàùµ, .'-]*"
                placeholder="Adres van de klant"
                clearButton>
              </ListInput>

              <ListInput
                id="klantGemeente"
                label="Gemeente:"
                defaultValue={details[5] || newGemeente}
                onChange={(event) => {setNewGemeente(event.target.value)}}
                type="text"
                errorMessage="Geen vreemde tekens aub!"
                required
                onInputClear={() => {setNewGemeente("")}}
                validate
                minlength={3}
                pattern="[a-zA-Z0-9éèçàùµ, .'-]*"
                placeholder="De gemeente van de klant"
                clearButton>
              </ListInput>

              <ListInput
                id="klantTelefoon"
                label="Telefoon:"
                defaultValue={details[6] || newTelefoon}
                onChange={(event) => {setNewTelefoon(event.target.value)}}
                type="tel"
                errorMessage="Voer een geldig telefoonnummer in aub!"
                required
                minlength={10}
                maxlength={12}
                validate
                pattern="[0-9+]*"
                onInputClear={() => {setNewTelefoon("")}}
                placeholder="De telefoonnummer van de klant"
                clearButton>
              </ListInput>

              <ListInput
                id="klantEmail"
                label="E-mail:"
                defaultValue={details[7] || newEmail}
                onChange={(event) => {setNewEmail(event.target.value)}}
                type="email"
                errorMessage="Voer een geldig email in aub!"
                required
                validate
                onInputClear={() => {setNewEmail("")}}
                pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
                placeholder="Email van de klant"
                clearButton>
              </ListInput>

              <ListItem id="klantBtw" title="BTW" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewBtw(e.target.value)}} name="BTW" value={details[8] || newBtw}>
                  <option value=""></option>
                  <option value="0%">0%</option>
                  <option value="6%">6%</option>
                  <option value="12%">12%</option>
                  <option value="21%">21%</option>
                </select>
              </ListItem>
            </List>

            <Button fill popupClose={true} onClick={() => UpdateProject(props.id)}>Bewerken</Button>
          </Block>
        </Page>
      </Popup>

      {/* Product met details weergeven */}
      <Popup
        id="details-popup"
        opened={popupOpened2}
        onPopupClosed={() => setPopupOpened2(false)}>
        <Page>
          <Navbar title="Product Informatie">
            <NavRight>
              <Block>
                <Link fill popupOpen=".bewerkProduct-popup" iconOnly={true}>
                  <Icon f7="pencil_ellipsis_rectangle"></Icon>
                </Link>
              </Block>
              <Link popupClose>Sluit</Link>
            </NavRight>
          </Navbar>
          <Block>
            <List noHairlinesMd> {/* <Icon slot="media" icon="demo-list-icon"></Icon> */}

              <ListItem header="Type" swipeout title={detailsOfInputProduct[0]}></ListItem>

              <ListItem header="Aantal" swipeout title={detailsOfInputProduct[1]}></ListItem>

              <ListItem header="Motor" swipeout title={detailsOfInputProduct[2]}></ListItem>

              <ListItem header="Kleur" swipeout title={detailsOfInputProduct[3]}></ListItem>

              <ListItem header="Gebouw" swipeout title={detailsOfInputProduct[4]}></ListItem>

              <ListItem header="Gebouw2" swipeout title={detailsOfInputProduct[5]}></ListItem>

              <ListItem header="Parking" swipeout title={detailsOfInputProduct[6]}></ListItem>

              <ListItem header="Montage" swipeout title={detailsOfInputProduct[7]}></ListItem>

              <ListItem header="Vliegraam" swipeout title={detailsOfInputProduct[8]}></ListItem>

              <ListItem header="Ballustrade" swipeout title={detailsOfInputProduct[9]}></ListItem>

              <ListItem header="Bevestiging" swipeout title={detailsOfInputProduct[10]}></ListItem>

              <ListItem header="Ladders" swipeout title={detailsOfInputProduct[11]}></ListItem>

              <ListItem header="Ladderhoogte" swipeout title={detailsOfInputProduct[12]}></ListItem>

              <ListItem header="Verdiep" swipeout title={detailsOfInputProduct[13]}></ListItem>

              <ListItem header="Kabels" swipeout title={detailsOfInputProduct[14]}></ListItem>

              <ListItem header="Kabelafwerking" swipeout title={detailsOfInputProduct[15]}></ListItem>

              <ListItem header="Automatisatie" swipeout title={detailsOfInputProduct[16]}></ListItem>

              <ListItem header="Bereikbaarheid" swipeout title={detailsOfInputProduct[17]}></ListItem>

              <ListItem header="Opmerking" swipeout title={detailsOfInputProduct[18]}></ListItem>

              {getAfbeeldingen()}

              <PhotoBrowser theme="dark" photos={images} ref={standalone}/>
            </List>
          </Block>
        </Page>
      </Popup>

      {/* Popup om details te bewerken van product */}
      <Popup
        className="bewerkProduct-popup"
        opened={popupOpened3}
        onPopupClosed={() => setPopupOpened3(false)}>
        <Page>
          <Navbar title="Product Bewerken">
            <NavRight>
              <Link popupClose>Sluit</Link>
            </NavRight>
          </Navbar>
          <Block>
            <List noHairlinesMd>

              <ListItem id="productNaam" title="Product" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewProduct(e.target.value)}} name="Product" value={detailsOfInputProduct[19] || newProduct}>
                  <option value=""></option>
                  <option value="Rolluik">Rolluik</option>
                  <option value="Screen(zip)">Screen(zip)</option>
                  <option value="Zonnetent">Zonnetent</option>
                  <option value="Uitvalscherm">Uitvalscherm</option>
                  <option value="Binnenzonwering">Binnenzonwering</option>
                  <option value="Vliegenraam">Vliegenraam</option>
                  <option value="Vliegenschuifdeur">Vliegenschuifdeur</option>
                </select>
              </ListItem>

              <ListInput
                id="productType"
                label="Type:"
                defaultValue={detailsOfInputProduct[0] || newType}
                onBlur={(event) => {setNewType(event.target.value)}}
                type="textarea"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <ListInput
                id="productAantal"
                label="Aantal:"
                defaultValue={detailsOfInputProduct[1] || newAantal}
                onBlur={(event) => {setNewAantal(event.target.value)}}
                type="number"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <ListItem id="productMotor" title="Motor" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewMotor(e.target.value)}} name="Motor" value={detailsOfInputProduct[2] || newMotor}>
                  <option value=""></option>
                  <option value="Somfy LT">Somfy LT</option>
                  <option value="Somfy IO">Somfy IO</option>
                  <option value="Somfy RTS">Somfy RTS</option>
                  <option value="Zaffer">Zaffer</option>
                  <option value="Zaffer AIO">Zaffer AIO</option>
                  <option value="Solar Somfy">Solar Somfy</option>
                  <option value="Solar GLR">Solar GLR</option>
                </select>
              </ListItem>

              <ListInput
                id="productKleur"
                label="Kleur:"
                defaultValue={detailsOfInputProduct[3] || newKleur}
                onBlur={(event) => {SetNewKleur(event.target.value)}}
                type="textarea"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <ListItem id="productGebouw" title="Gebouw" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewGebouw(e.target.value)}} name="Gebouw" value={detailsOfInputProduct[4] || newGebouw}>
                  <option value=""></option>
                  <option value="Huis">Huis</option>
                  <option value="Klein gebouw">Klein gebouw</option>
                  <option value="Groot gebouw">Groot gebouw</option>
                  <option value="Hoog gebouw">Hoog gebouw</option>
                </select>
              </ListItem>

              <ListItem id="productGebouw2" title="Gebouw 2" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewGebouw2(e.target.value)}} name="Gebouw 2" value={detailsOfInputProduct[5] || newGebouw2}>
                  <option value=""></option>
                  <option value="Bestaand">Bestaand</option>
                  <option value="Nieuwbouw">Nieuwbouw</option>
                  <option value="Renovatie">Renovatie</option>
                </select>
              </ListItem>

              <ListItem id="productParking" title="Parking" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewParking(e.target.value)}} name="Parking" value={detailsOfInputProduct[6] || newParking}>
                  <option value=""></option>
                  <option value="Makkelijk">Makkelijk</option>
                  <option value="Moeilijk">Moeilijk</option>
                  <option value="Parking">Parking</option>
                </select>
              </ListItem>

              <ListItem id="productMontage" title="Montage" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewMontage(e.target.value)}} name="Montage" value={detailsOfInputProduct[7] || newMontage}>
                  <option value=""></option>
                  <option value="In de dag">In de dag</option>
                  <option value="Op de dag">Op de dag</option>
                  <option value="Nis">Nis</option>
                  <option value="Op chassis">Op chassis</option>
                </select>
              </ListItem>

              <ListItem id="productVliegenraam" title="Vliegenraam aanwezig" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewVliegraam(e.target.value)}} name="Vliegenraam aanwezig" value={detailsOfInputProduct[8] || newVliegraam}>
                  <option value=""></option>
                  <option value="Nee">Nee</option>
                  <option value="Ja">Ja</option>
                </select>
              </ListItem>

              <ListItem id="productBallustrade" title="Ballustrade aanwezig" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewBallustrade(e.target.value)}} name="Ballustrade aanwezig" value={detailsOfInputProduct[9] || newBallustrade}>
                  <option value=""></option>
                  <option value="Nee">Nee</option>
                  <option value="Ja">Ja</option>
                </select>
              </ListItem>

              <ListItem id="productBevestiging" title="Bevestiging in" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewBevestiging(e.target.value)}} name="Bevestiging in" value={detailsOfInputProduct[10] || newBevestiging}>
                  <option value=""></option>
                  <option value="Hout">Hout</option>
                  <option value="Steen">Steen</option>
                  <option value="Alu">Alu</option>
                  <option value="Staal">Staal</option>
                  <option value="Inox">Inox</option>
                  <option value="Gyproc">Gyproc</option>
                  <option value="Crepi">Crepi</option>
                </select>
              </ListItem>

              <ListItem id="productLadders" title="Ladders" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewLadders(e.target.value)}} name="Ladders" value={detailsOfInputProduct[11] || newLadders}>
                  <option value=""></option>
                  <option value="Schuifladder">Schuifladder</option>
                  <option value="Normale ladders">Normale ladders</option>
                  <option value="Stelling">Stelling</option>
                  <option value="Hoogwerker">Hoogwerker</option>
                </select>
              </ListItem>

              <ListInput
                id="productHoogteLadders"
                label="Hoogte ladders:"
                defaultValue={detailsOfInputProduct[12] || newLaddersHoogte}
                onBlur={(event) => {setNewLaddersHoogte(event.target.value)}}
                type="textarea"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <ListItem id="productVerdiep" title="Verdiep" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewVerdiep(e.target.value)}} name="Verdiep" value={detailsOfInputProduct[13] || newVerdiep}>
                  <option value=""></option>
                  <option value="Gelijksvloer">Gelijksvloer</option>
                  <option value="1ste verdiep">1ste verdiep</option>
                  <option value="2de verdiep">2de verdiep</option>
                  <option value="3de verdiep">3de verdiep</option>
                </select>
              </ListItem>

              <ListItem id="productKabel" title="Kabel" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewKabel(e.target.value)}} name="Kabel" value={detailsOfInputProduct[14] || newKabel}>
                  <option value=""></option>
                  <option value="Muurdoorboring">Muurdoorboring</option>
                  <option value="Door chassis">Door chassis</option>
                  <option value="Voorzien door klant (kabel of buis)">Voorzien door klant (kabel of buis)</option>
                  <option value="Via geleider">Via geleider</option>
                  <option value="Op de kast">Op de kast</option>
                </select>
              </ListItem>

              <ListItem id="productKabelafwerking" title="Kabelafwerking" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewKabelafwerking(e.target.value)}} name="Kabelafwerking" value={detailsOfInputProduct[15] || newKabelafwerking}>
                  <option value=""></option>
                  <option value="Kabelgoot (kleur)">Kabelgoot (kleur)</option>
                  <option value="Buis">Buis</option>
                  <option value="Slijpen in muur">Slijpen in muur</option>
                  <option value="Voorzien door klant">Voorzien door klant</option>
                </select>
              </ListItem>

              <ListItem id="productAutomatisatie" title="Automatisatie" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewAutomatisatie(e.target.value)}} name="Automatisatie" value={detailsOfInputProduct[16] || newAutomatisatie}>
                  <option value=""></option>
                  <option value="3D wind">3D wind</option>
                  <option value="Zonnecel sunis wirefree IO">Zonnecel sunis wirefree IO</option>
                  <option value="Wind wire free IO">Wind wire free IO</option>
                  <option value="Soliris sensor RTS">Soliris sensor RTS</option>
                  <option value="Eolis sensor RTS">Eolis sensor RTS</option>
                  <option value="Situo 5 IO">Situo 5 IO</option>
                  <option value="Situo 5 variation">Situo 5 variation</option>
                  <option value="Smoove IO">Smoove IO</option>
                  <option value="Telis 1">Telis 1</option>
                  <option value="Telis 4">Telis 4</option>
                  <option value="Smoove RTS">Smoove RTS</option>
                  <option value="Zaffer zender">Zaffer zender</option>
                  <option value="Zon/wind">Zon/wind</option>
                </select>
              </ListItem>

              <ListItem id="productBereikbaarheid" title="Bereikbaarheid" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewBereikbaarheid(e.target.value)}} name="Bereikbaarheid" value={detailsOfInputProduct[17] || newBereikbaarheid}>
                  <option value=""></option>
                  <option value="Lift">Lift</option>
                  <option value="Verhuislift">Verhuislift</option>
                  <option value="Gelijksvloer">Gelijksvloer</option>
                  <option value="Gelijksvloer: opgepast met lengte">Gelijksvloer: opgepast met lengte</option>
                </select>
              </ListItem>

              <ListInput
                id="productOpmerkingen"
                label="Opmerkingen:"
                defaultValue={detailsOfInputProduct[18] || newOpmerking}
                onBlur={(event) => {setNewOpmerking(event.target.value)}}
                type="textarea"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <Button fill className='btnVerzendProject' popupOpen="#AddAfbeelding-popup">Afbeelding Toevoegen</Button>
              <Button fill className='btnVerzendProject' onClick={() => UpdateProduct(productId)}>Bewerken</Button>
            </List>
          </Block>
        </Page>
      </Popup>

      {/* Voeg product afbeelding toe via popup */}
      <Popup
        id="AddAfbeelding-popup"
        opened={popupOpened4}
        onPopupClosed={() => setPopupOpened4(false)}>
        <Page>
          <Navbar title="Afbeelding Toevoegen">
            <NavRight>
              <Link popupClose>Sluit</Link>
            </NavRight>
          </Navbar>
          <Block>
          <Input type="file" accept="image/*" capture="camera" onChange={handleChange} className="inputtypeImage"></Input>
            <Button onClick={uploadImage} disabled={disable}>Upload foto</Button>
            <p>{isProgress} %</p>
            <Progressbar color="blue" progress={isProgress}/>
            <div>
              <img src={url} className="imgPost"></img>
            </div>
          </Block>
          <Button className='btnVerzendProject' onClick={() => addExtraImageForProduct()} disabled={disable2} large fill>Afbeelding Toevoegen</Button>
        </Page>
      </Popup>

      {/* Extra product toevoegen */}
      <Popup
        className="AddExtraProduct-popup"
        opened={popupOpened5}
        onPopupClosed={() => setPopupOpened5(false)}>
        <Page>
          <Navbar title="Extra Product Toevoegen">
            <NavRight>
              <Link popupClose>Sluit</Link>
            </NavRight>
          </Navbar>
          <Block>
            <List noHairlinesMd>
              <ListItem id="productNaam" title="Product" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewProduct(e.target.value)}} name="Product">
                  <option value=""></option>
                  <option value="Rolluik">Rolluik</option>
                  <option value="Screen(zip)">Screen(zip)</option>
                  <option value="Zonnetent">Zonnetent</option>
                  <option value="Uitvalscherm">Uitvalscherm</option>
                  <option value="Binnenzonwering">Binnenzonwering</option>
                  <option value="Vliegenraam">Vliegenraam</option>
                  <option value="Vliegenschuifdeur">Vliegenschuifdeur</option>
                </select>
              </ListItem>

              <ListInput
                id="productType"
                label="Type:"
                onBlur={(event) => {setNewType(event.target.value)}}
                type="textarea"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <ListInput
                id="productAantal"
                label="Aantal:"
                onBlur={(event) => {setNewAantal(event.target.value)}}
                type="number"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <ListItem id="productMotor" title="Motor" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewMotor(e.target.value)}} name="Motor">
                  <option value=""></option>
                  <option value="Somfy LT">Somfy LT</option>
                  <option value="Somfy IO">Somfy IO</option>
                  <option value="Somfy RTS">Somfy RTS</option>
                  <option value="Zaffer">Zaffer</option>
                  <option value="Zaffer AIO">Zaffer AIO</option>
                  <option value="Solar Somfy">Solar Somfy</option>
                  <option value="Solar GLR">Solar GLR</option>
                </select>
              </ListItem>

              <ListInput
                id="productKleur"
                label="Kleur:"
                onBlur={(event) => {SetNewKleur(event.target.value)}}
                type="textarea"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <ListItem id="productGebouw" title="Gebouw" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewGebouw(e.target.value)}} name="Gebouw">
                  <option value=""></option>
                  <option value="Huis">Huis</option>
                  <option value="Klein gebouw">Klein gebouw</option>
                  <option value="Groot gebouw">Groot gebouw</option>
                  <option value="Hoog gebouw">Hoog gebouw</option>
                </select>
              </ListItem>

              <ListItem id="productGebouw2" title="Gebouw 2" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewGebouw2(e.target.value)}} name="Gebouw 2">
                  <option value=""></option>
                  <option value="Bestaand">Bestaand</option>
                  <option value="Nieuwbouw">Nieuwbouw</option>
                  <option value="Renovatie">Renovatie</option>
                </select>
              </ListItem>

              <ListItem id="productParking" title="Parking" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewParking(e.target.value)}} name="Parking">
                  <option value=""></option>
                  <option value="Makkelijk">Makkelijk</option>
                  <option value="Moeilijk">Moeilijk</option>
                  <option value="Parking">Parking</option>
                </select>
              </ListItem>

              <ListItem id="productMontage" title="Montage" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewMontage(e.target.value)}} name="Montage">
                  <option value=""></option>
                  <option value="In de dag">In de dag</option>
                  <option value="Op de dag">Op de dag</option>
                  <option value="Nis">Nis</option>
                  <option value="Op chassis">Op chassis</option>
                </select>
              </ListItem>

              <ListItem id="productVliegenraam" title="Vliegenraam aanwezig" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewVliegraam(e.target.value)}} name="Vliegenraam aanwezig">
                  <option value=""></option>
                  <option value="Nee">Nee</option>
                  <option value="Ja">Ja</option>
                </select>
              </ListItem>

              <ListItem id="productBallustrade" title="Ballustrade aanwezig" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewBallustrade(e.target.value)}} name="Ballustrade aanwezig">
                  <option value=""></option>
                  <option value="Nee">Nee</option>
                  <option value="Ja">Ja</option>
                </select>
              </ListItem>

              <ListItem id="productBevestiging" title="Bevestiging in" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewBevestiging(e.target.value)}} name="Bevestiging in">
                  <option value=""></option>
                  <option value="Hout">Hout</option>
                  <option value="Steen">Steen</option>
                  <option value="Alu">Alu</option>
                  <option value="Staal">Staal</option>
                  <option value="Inox">Inox</option>
                  <option value="Gyproc">Gyproc</option>
                  <option value="Crepi">Crepi</option>
                </select>
              </ListItem>

              <ListItem id="productLadders" title="Ladders" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewLadders(e.target.value)}} name="Ladders">
                  <option value=""></option>
                  <option value="Schuifladder">Schuifladder</option>
                  <option value="Normale ladders">Normale ladders</option>
                  <option value="Stelling">Stelling</option>
                  <option value="Hoogwerker">Hoogwerker</option>
                </select>
              </ListItem>

              <ListInput
                id="productHoogteLadders"
                label="Hoogte ladders:"
                onBlur={(event) => {setNewLaddersHoogte(event.target.value)}}
                type="textarea"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <ListItem id="productVerdiep" title="Verdiep" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewVerdiep(e.target.value)}} name="Verdiep">
                  <option value=""></option>
                  <option value="Gelijksvloer">Gelijksvloer</option>
                  <option value="1ste verdiep">1ste verdiep</option>
                  <option value="2de verdiep">2de verdiep</option>
                  <option value="3de verdiep">3de verdiep</option>
                </select>
              </ListItem>

              <ListItem id="productKabel" title="Kabel" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewKabel(e.target.value)}} name="Kabel">
                  <option value=""></option>
                  <option value="Muurdoorboring">Muurdoorboring</option>
                  <option value="Door chassis">Door chassis</option>
                  <option value="Voorzien door klant (kabel of buis)">Voorzien door klant (kabel of buis)</option>
                  <option value="Via geleider">Via geleider</option>
                  <option value="Op de kast">Op de kast</option>
                </select>
              </ListItem>

              <ListItem id="productKabelafwerking" title="Kabelafwerking" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewKabelafwerking(e.target.value)}} name="Kabelafwerking">
                  <option value=""></option>
                  <option value="Kabelgoot (kleur)">Kabelgoot (kleur)</option>
                  <option value="Buis">Buis</option>
                  <option value="Slijpen in muur">Slijpen in muur</option>
                  <option value="Voorzien door klant">Voorzien door klant</option>
                </select>
              </ListItem>

              <ListItem id="productAutomatisatie" title="Automatisatie" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewAutomatisatie(e.target.value)}} name="Automatisatie">
                  <option value=""></option>
                  <option value="3D wind">3D wind</option>
                  <option value="Zonnecel sunis wirefree IO">Zonnecel sunis wirefree IO</option>
                  <option value="Wind wire free IO">Wind wire free IO</option>
                  <option value="Soliris sensor RTS">Soliris sensor RTS</option>
                  <option value="Eolis sensor RTS">Eolis sensor RTS</option>
                  <option value="Situo 5 IO">Situo 5 IO</option>
                  <option value="Situo 5 variation">Situo 5 variation</option>
                  <option value="Smoove IO">Smoove IO</option>
                  <option value="Telis 1">Telis 1</option>
                  <option value="Telis 4">Telis 4</option>
                  <option value="Smoove RTS">Smoove RTS</option>
                  <option value="Zaffer zender">Zaffer zender</option>
                  <option value="Zon/wind">Zon/wind</option>
                </select>
              </ListItem>

              <ListItem id="productBereikbaarheid" title="Bereikbaarheid" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
                <select onChange={(e) => {setNewBereikbaarheid(e.target.value)}} name="Bereikbaarheid">
                  <option value=""></option>
                  <option value="Lift">Lift</option>
                  <option value="Verhuislift">Verhuislift</option>
                  <option value="Gelijksvloer">Gelijksvloer</option>
                  <option value="Gelijksvloer: opgepast met lengte">Gelijksvloer: opgepast met lengte</option>
                </select>
              </ListItem>

              <ListInput
                id="productOpmerkingen"
                label="Opmerkingen:"
                onBlur={(event) => {setNewOpmerking(event.target.value)}}
                type="textarea"
                resizable
                placeholder=""
                clearButton>
              </ListInput>

              <Button className='btnVerzendProject' onClick={addExtraProductForProject} popupOpen="#AddNewAfbeelding-popup" large fill>Voeg Afbeeldingen Toe</Button>
            </List>
          </Block>
        </Page>
      </Popup>

      {/* Voeg product afbeelding toe via popup */}
      <Popup
        id="AddNewAfbeelding-popup"
        opened={popupOpened6}
        onPopupClosed={() => setPopupOpened6(false)}>
        <Page>
          <Navbar title="Afbeelding Toevoegen">
            <NavRight>
              <Link popupClose>Sluit</Link>
            </NavRight>
          </Navbar>
          <Block>
          <Input type="file" accept="image/*" capture="camera" onChange={handleChange} className="inputtypeImage"></Input>
            <Button onClick={uploadImage} disabled={disable}>Upload foto</Button>
            <p>{isProgress} %</p>
            <Progressbar color="blue" progress={isProgress}/>
            <div>
              <img src={url} className="imgPost"></img>
            </div>
          </Block>
          <Button className='btnVerzendProject' onClick={() => addExtraImageForNewProduct()} disabled={disable2} large fill>Afbeelding Toevoegen</Button>
        </Page>
      </Popup>
    </Page>
  )
}
export default DetailsProjectList;