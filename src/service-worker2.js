//bronnen: https://github.com/udacity/ud811 -- https://developers.google.com/web/fundamentals/primers/service-workers
//cache naam
var cacheName = 'shadowmaker-v1';

//files die in de cache opgeslagen moeten worden
var filesToCache = [
  '/',
  '/index.html',
  '/manifest.json',
  '/css/app.css',
  '/css/icons.css',
  '/js/app.js',
  '/js/firebase.js',
  '/js/routes.js',
  '/js/store.js',
  '/pages/404.jsx',
  '/pages/about.jsx',
  '/pages/catalog.jsx',
  '/pages/dynamic-route.jsx',
  '/pages/form.jsx',
  '/pages/home.jsx',
  '/pages/product.jsx',
  '/pages/request-and-load.jsx',
  '/pages/settings.jsx',
  '/components/app.jsx',
  '/components/AanmakenProjectIcon.jsx',
  '/components/DetailsProjectList.jsx',
  '/components/FirstAddProjectModal.jsx',
  '/components/projectList.jsx',
  '/components/WebcamCapture.jsx',
  '/icons/128x128.png',
  '/icons/144x144.png',
  '/icons/152x152.png',
  '/icons/192x192.png',
  '/icons/256x256.png',
  '/icons/512x512.png',
  '/icons/apple-touch-icon.png',
  '/icons/favicon.png'
];

//Install a service worker
self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
});

//Update a service worker
self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
});

//Cache and return requests
self.addEventListener('fetch', function(e) {
  console.log('[ServiceWorker] Fetch', e.request.url);
  e.respondWith(
    caches.match(e.request).then(function(response) {
      return response || fetch(e.request);
    })
  );
});

// fetch event
/*self.addEventListener('fetch', evt => {
  // check if request is made by chrome extensions or web page
  // if request is made for web page url must contains http.
  if (!(evt.request.url.indexOf('http') === 0)) return; // skip the request if request is not made with http protocol

  evt.respondWith(
    caches.match(evt.request)
      .then(
        cacheRes =>
          cacheRes ||
          fetch(evt.request).then(fetchRes =>
            caches.open(dynamicNames).then(cache => {
              cache.put(evt.request.url, fetchRes.clone());
              return fetchRes;
            })
          )
      )
      .catch(function() {
        //wanneer een file niet kan gevonden worden in de cache
        return caches.match('offline.html');
      })
  );
});*/

/*
//cacht ALLES op - https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers 
self.addEventListener('fetch', (event) => {
  event.respondWith(
    caches.match(event.request).then((resp) => {
      return resp || fetch(event.request).then((response) => {
        let responseClone = response.clone();
        caches.open(cacheName).then((cache) => {
          cache.put(event.request, responseClone);
        });
        return response;
      }).catch(() => {
        return caches.match('/index.html');
      })
    }));
});
*/
