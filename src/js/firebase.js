//Firebase
import { initializeApp } from 'firebase/app';
import { getFirestore, collection, getDocs } from 'firebase/firestore/lite';

//Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDqcQyhGE5MXMS0bTb9y-_NgEitwRYf_r8",
  authDomain: "shadowmaker-82b93.firebaseapp.com",
  projectId: "shadowmaker-82b93",
  storageBucket: "shadowmaker-82b93.appspot.com",
  messagingSenderId: "302524693982",
  appId: "1:302524693982:web:5ca9e54699821dcdb4714f",
  measurementId: "G-V9B33GZR83"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
