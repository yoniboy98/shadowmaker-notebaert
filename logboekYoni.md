Logboek Yoni Vindelinckx


domein: www.yonivindelinckx.be

- Intro to Progressive Web Apps op Udacity (https://classroom.udacity.com/courses/ud811) (Github: https://github.com/udacity/ud811)
    - Les 2: 1u

Les 2 - 01/10
- Aanmaken gemeenschappelijk github project
- Informatie project opschrijven - 15min
- Gesprek leerkracht - 15min

08/10  10u - 16u 
- UI aanmaken
- firebase toevoegen in de app

15/10  10u - 17u 
- service worker troubleshooten
- aanmaken van verschillende schermen om projecten toe te voegen en projecten te tonen

22/10  10u - 19u
- firebase toevoegen (troubleshooten)
   - het lukt niet op firebase te implementeren in onze app

- firestore document aanmaken (een project) in firebase zelf

23/10  10u - 19u
- firebase toegevoegd in de app 
- het tonen van projecten uit firestore in de app

26/10  12u - 13u
- aangemaakte projecten verwijderen
  - dit werkt nog niet helemaal, het lijkt of projecten verwijdert worden maar dit gebeurd vreemd genoeg niet

27/10  14u - 16u
- project updaten door middel van een popup
- herbruikbare componenten aanmaken in het mapje components 
  - deze componenten zijn popups dat getoond worden voor het toevoegen van een project in de app

28/10 19u - 21u30
- details scherm aanmaken als component

29/10 18u - 19u41

- details tonen van een project op id via props in de listitem

30/10 13u - 18u30

-  alle details ophalen van producten op id, in een hook state zetten en tonen op het scherm

1/11 15u - 16u

- component aanmaken met een icon onclick om projecten te accepteren (popup) te openen.


3/11 10u - 15u

- updaten afwerken 
- confirmtext bij het verwijderen troubleshooten
   - projecten werden niet verwijdert, ookal leek het zo vanege de animatie


10/11 10u - 12u

- troubleshooten refresh 


8/12 12u - 17u30

- data flow van de applicatie goedzetten, door het testen in functionaliteit was de opbouw slordig

10/12  11u - 14u

- subcollectie aanmaken in firestore 
   - deze subcollectie gaat afbeeldingen tonen van elk product
      - door een subcollectie te maken heb je de mogelijkheid om oneindig veel afbeelding toe te voegen.

- photobrowser toevoegen in de app

18/12  11u - 13u

- troubleshooting photobrowser en datum 
  -  photobrowser kan nog niet alle afbeeldingen tonen, maximum 1
  - datum wordt niet goed opgeslagen in localstorage

19/12  16u - 18u

- fixed photobrowser 
  - de push functie in Javascript werd in een loop uitgevoerd
    - dit moest uit de loop gebeuren en vervolgens in een hook state te laden
 - troubleshooting datum
  - het probleem situeerde zich met de type van de listinput dat op date werd gezet, waardoor een datum object werd aangemaakt en deze niet goed begrepen kon worden door localstorage

28/12  14u - 20u

- verschillende producten weergeven voor projecten 
   - tot hiervoor kon er maar 1 product per projecten aangemaakt worden 

29/12  12u - 18u

- bewerken van alle producten 

30/12  14u - 19u

- afbeelding toevoegen voor bestaande producten
- route naar klant dat geopend wordt met google maps
     - op dit moment wordt alleen nog maar google maps geopend met de value undifiend

31/12  14u - 19u

- details afwerken van de applicatie

8/01   12u - 15u
 
- adres meegevan aan google maps zodat de route meteen berekend wordt

9/01   12u - 19u
 
- UI verbeteren en moderner maken met css

12/01   14u - 19u
 
- smart select probleem oplossen.
   - Nu worden de geselecteerde values van een product aangeduid in de smart select.

14/01   14u - 15u
- Alles testen
- Offline alles testen op de gsm en laptop
- Laatste build maken

20/01 - 12u - 15u
- Voorbereiden van de presentatie
- Kleine aanpassing in de documentatie
- Kleine aanpassingen in de code
- Test data toevoegen
- warnings in de app weghalen
21/01
- Examen presentatie