Les 1 - 24/09
- Intro + uitleg: 1u15
- Puntje per puntje lezen en kennis opdoen (https://web.dev/progressive-web-apps/) - 30min
- Codelab maken (https://developers.google.com/codelabs/pwa-training/pwa03--going-offline#0) - 20min
- Intro to Progressive Web Apps op Udacity (https://classroom.udacity.com/courses/ud811) (Github: https://github.com/udacity/ud811)
    - Les 1: 1u

27/09
- Intro to Progressive Web Apps op Udacity (https://classroom.udacity.com/courses/ud811) (Github: https://github.com/udacity/ud811)
    - Les 2: 1u

Les 2 - 01/10
- Aanmaken gemeenschappelijk github project
- project aanmaken met framework7 react - 10min
- project bekijken en proberen begrijpen - 20min
- Informatie project opschrijven - 10min
- Gesprek leerkracht - 15min

Les 3 - 08/10 - 5u
- GUI maken
    - Home
        - Input fields & options
    - Catalog
        - Lijst van projecten
            - Details
- Firebase toevoegen aan project

09/10 - 1u30
- Service-worker toevoegen

Les 4 - 15/10
- Service worker probleem oplossen - 2u
- Left panel met navigatie - 15min
    - Nog niet gelukt
- Value of input fields toevoegen aan firebase - 1u
    - Nog niet gelukt

21/10 - 1u
- Service-worker oplossen

Les 5 - 22/10 - 7u
- Service-worker verder oplossen - 1u
- Firestore verder toevoegen (https://firebase.google.com/docs/web/setup) - 4u
    - Data ophalen in een lijst - 1u
    - Data toevoegen aan db - 1u
- Left panel met navigatie weg laten (onnodig) - 1min

Les 6 - 29/10
- Data toevoegen aan Firestore - 1u
- Schermen verder uitwerken - 2u

30/10 - 3u
- Home screen aanpassen
- Data toevoegen aan Firestore
- Foto maken met react

11/11 - 2u
- Aanpassen lay-out van bepaalde files
- Code aanpassen zodat deze overzichtelijker worden
- Commentaar schrijven bij de code
- Uittesten van het hele project

Wat werkt er tot nu toe:
1)
- Project aanmaken
2) 
- Projecten ophalen (alfabetische volgorde -> later op datum (laatste aangemaakt vanboven))
- Project verwijderen
- Project details bekijken
- Project details bewerken
3)
- Foto maken

12/11 - 4u
- LocalStorage informatie (inputvelden opslaan en ophalen)

19/11 - 8u
- LocalStorage informatie afmaken - volledig inorde
- ListInput velden aanpassen - volledig inorde

26/11 - 4u
- Uploaden van foto's en deze doorsturen naar de db
- Probleempje met de branch oplossen - inorde

3/12 - 8u
- Layout aanpassen
- Producten toevoegen
- LocalStorage product aanmaken
- Service-worker aanpassen en proberen te laten werken
    - Testen met Lighthouse
- Online plaatsen
- React production build

8/12 - 2u
- Service-worker aanpassen en proberen te laten werken
- Online plaatsen oplossen

9/12 - 6u
- LocalStorage leegmaken wanneer een nieuw product (als subcollectie) moet toegevoegd worden
- Google maps toevoegen (tab 3)
- Webgradient toevoegen
- Build + online fouten eruit halen

10/12 - 4u
- Build fout proberen oplossen
- Tab 3 zal een lijst worden van klanten met link naar google maps voor de route

16/12 - 6u
- Inputvelden leegmaken na toevoegen aan database
- Meerdere producten aanmaken
- Meerdere foto's toevoegen aan een product
- Kleine verbeteringen

17/12 - 4u
- Documentatie maken
- Lijst van klanten
- Klanten bewerken
- Lijst van projecten van de klant

22/12 - 4u
- ListInput leegmaken bij button click
- Inactief maken van buttons wanneer ze niet nodig zijn (foto's)
- Tonen hoeveelste product wordt toegevoegd
- Lijst projecten/klanten alfabetisch sorteren (niet gelukt)
- Build proberen oplossen

24/12 - 1u
- Tonen hoeveelste product wordt aangemaakt
- Nieuw project aanmaken zodat de build kan getest worden in een ander project
- Afgewerkt / Niet afgewerkt (filters - kleurtjes) (canvas: later)

04/01 - 3u
- Aanpassingen LIJST PROJECTEN page
    - Kleine aanpassingen
    TODO:
    - min en max lengte worden niet gecontroleerd bij telefoon
    - .be moet niet bij email
    - 2de popup niet tonen na 2de click button project aanmaken
    - Pas valideren na onblur?
    - Inputvelden willen niet clearen na project aanmaken
    - Kan je er niet voor zorgen dat bij smart select alles te zien is zonder te moeten scrollen
    - Datum aangemaakt toevoegen?
- Aanpassingen PROJECTEN page
    - Kleine aanpassingen
    - Bookmark list aanmaken
    - Bookmark toevoegen of verwijderen van project
    - Projecten sorteren op datum
    TODO
    - Icon toevoegen aan bookmark
    - Datum omvormen
    - Projecten toevoegen aan afgewerkt of nog bezig (ipv alle projecten in 1 lijst)

05/01 - 3u
- Aanpassingen PROJECTEN page
    - Adres toevoegen aan de details
    - ListInput van bewerk updaten naar laatste versie (klant en product)
    TODO
    - Value van select wordt niet aangeduid bij bewerken (klant en product)
    - Controleren dat product naam niet leeg is bij toevoegen product (al de rest mag leeg zijn)
    - Swipe voor verwijderen zorgt ervoor dat de tabs ook bewegen
- Aanpassingen KLANTEN page
    - Kleine aanpassingen

06/01 - 3u
- Builden in een nieuw project
- Service worker wilt niet werken in het nieuwe project

07/01 - 2u
- Builden en Service Worker werken

08/01 - 3u
- Documentatie
- TODO verder uitwerken
- TODO HOME
    - .be moet niet bij email
    - Controleren dat er geen fouten zijn
    - Inputvelden willen niet clearen na project aanmaken
- TODO PROJECTEN
    - Swipe voor verwijderen zorgt ervoor dat de tabs ook bewegen
    - Specifieke afbeeldingen verwijderen (als er bv een slechte foto bij een product is toegevoegd)
    - Value van select wordt niet aangeduid bij bewerken (klant en product)
- TODO KLANTEN
    - 

11/01, 12/01 en 13/01 - 6u
- TODO verder uitwerken
    - .be moet niet bij email - OK
    - Controleren dat er geen fouten zijn - OK
    - Inputvelden willen niet clearen na project aanmaken - OK
    - Value van select wordt niet aangeduid bij bewerken (klant en product) - OK
- TODO HOME
    - 
- TODO PROJECTEN
    - Value van select wordt niet getoont bij bewerken (klant en product)
    - Value van select wordt niet leeggemaakt na product aanmaken

14/01 - 1u
- Alles testen
- Offline alles testen
- Laatste build maken

20/01 - 4u
- Voorbereiden van de presentatie
- Kleine aanpassing in de documentatie
- Kleine aanpassingen in de code
- Test data toevoegen

21/01
- Examen presentatie